package com.aplombtechbd.smartmeter.tokenapi.validation;

import com.aplombtechbd.smartmeter.tokenapi.TokenImplementation;

public class Validator {

  public boolean isValidMeterNo(String meterNo) {
    String twelveDigitMeterNoRegx = "\\d{12}";
    String elevenDigitMeterNoRegx = "\\d{11}";
    return meterNo.matches(twelveDigitMeterNoRegx) || meterNo.matches(elevenDigitMeterNoRegx);
  }

  public boolean isValidSgc(String sgc) {
    String sixDigitSgcRegx = "\\d+";
    return sgc.matches(sixDigitSgcRegx);
  }

  public boolean isValidKeyVersionNumber(int krn) {
    String oneDigitKeyVersionNumRegx = "\\d{1}";
    String keyVersionNumber = String.valueOf(krn);
    return keyVersionNumber.matches(oneDigitKeyVersionNumRegx);
  }

  public boolean isValidKeyExpiredTime(int ken) {
    String oneDigitKeyExpiredTimeRegx = "\\d+";
    String keyExpiredTime = String.valueOf(ken);
    return ((ken <= 255) && (keyExpiredTime.matches(oneDigitKeyExpiredTimeRegx)));
  }

  public boolean isValidTariffIndex(int tariffIndex) {
    String TI = String.valueOf(tariffIndex);
    String oneDigitTariffIndexRegx = "\\d{1}";
    String twoDigitTariffIndexRegx = "\\d{2}";
    return (TI.matches(oneDigitTariffIndexRegx)) || (TI.matches(twoDigitTariffIndexRegx));
  }

  public boolean isValidKeyNo(long keyNo) {
    String keyNoString = String.valueOf(keyNo);
    return (keyNo >= 0 && keyNo <= 65535) && keyNoString.matches("\\d+");
  }

  public boolean isValidSeqNo(int seqNo) {
    String seqNoString = String.valueOf(seqNo);
    return (seqNo >= 1 && seqNo <= 255) && seqNoString.matches("\\d+");
  }

  public boolean isValidAmount(long amount) {
    String amountString = String.valueOf(amount);
    return (amount >= 0 && amount <= 99999999) && amountString.matches("\\d+");
  }

  public boolean isValidHour(int [] hours){
    if(hours.length > 4 || hours.length == 0)
      return false;
    else{
      for (int hr: hours) {
        if(hr < 0 || hr > 23)
          return false;
      }
    }
    return true;
  }

  public boolean isValidActivatingModel(int activationModel){
    return activationModel == 0 || activationModel == 1;
  }

  public boolean isValidDate(String date){
    String dateValidatingRegex = "\\d{4}-\\d{2}-\\d{2}";
    if(date.matches(dateValidatingRegex))
    {
      int year = Integer.parseInt(date.substring(2,4));
      int month = Integer.parseInt(date.substring(5, 7));
      int day = Integer.parseInt(date.substring(8, 10));
      return((year <= 99 && year >= 0) && (month <= 12 && month > 0) && (day <= 31 && day > 0));
    }
    else
      return false;
  }

  public boolean isValidMaxPowerLimit(int [] maxPower){
    if(maxPower.length > 4 || maxPower.length == 0)
      return false;
    else{
      for (int mxp: maxPower) {
        if(mxp < 1 || mxp > 2046)
          return false;
      }
    }
    return true;
  }

  public boolean isValidRates(int [] rates){
    if(rates.length > 9 || rates.length == 0)
      return false;
    else{
      for (int rate: rates) {
        if(rate < 1 || rate > 8191)
          return false;
      }
    }
    return true;
  }

  public boolean isValidValidate(int validate){
    return validate >= 0 && validate <= 7;
  }

  public boolean isValidTimes(int [] times){
    if(times.length > 9 || times.length == 0)
      return false;
    else{
      for (int time: times) {
        if(time < 0 || time > 23)
          return false;
      }
    }
    return true;
  }

  public boolean isValidSteps(int[] steps){
    if(steps.length > 8 || steps.length == 0)
      return false;
    else{
      for (int step: steps) {
        if(step < 0 || step > 4095)
          return false;
      }
    }
    return true;
  }

  public boolean isValidFlag(int[] flag) {
    TokenImplementation.StepTariffFlagErrorCode = 0;
    if (flag.length != 8)
    {
      TokenImplementation.StepTariffFlagErrorCode = 6;
      return false;
    }
    for(int b: flag)
    {
      if(b > 1 || b < 0){
        TokenImplementation.StepTariffFlagErrorCode = 6;
        return false;
      }
    }
    return true;
  }


  public boolean isValidHolidays(String[] holidays){
    if(holidays.length <= 0 || holidays.length > 99)
      return false;
    for (String date: holidays){
      if(!isValidDate(date))
        return false;
    }
    return true;
  }

  public boolean isValidHolidayMode(int holidayMode){
    return holidayMode == 1 || holidayMode == 0;
  }


  public boolean isValidNumOfAllowableDays(int allowableDays){
    return allowableDays <= 99 && allowableDays >= 0;
  }


  public boolean isValidFriendModeHours(int[] hours){
    if (hours.length != 2)
      return false;
    for (int hour: hours){
      if(hour > 23 || hour < 0)
        return false;
    }
    return true;
  }


  public boolean isValidFriendMode(int friendMode){
    return friendMode == 1 || friendMode == 0;
  }


  public boolean isValidFriendModeDays(int[] days){
    if(days.length <= 0 || days.length > 7)
      return false;

    for (int day: days){
      if(day < 0 || day > 6)
        return false;
    }
    return true;
  }

  public boolean isValidManufacturerID(int manufacturerID){
    return (manufacturerID >= 0 && manufacturerID <= 99);
  }

  public boolean isValidControl(int control){
    return (control >= 0 && control<=36);
  }


  public boolean isValidMeterMode(int meterMode){
    return (meterMode == 0 || meterMode == 1);
  }

  public boolean isValidSingleTariffRate(int rate) {
    return ((rate >= 1) && (rate <= 8191));
  }

  public boolean areCreditTokenParametersValid(String meterNo, String sgc, int krn, int ken, int tariffIndex, long keyNo, int seqNo, long amount) {
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidTariffIndex(tariffIndex) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidAmount(amount);
  }

  public boolean areClearTokenParametersValid(String meterNo, String sgc, int krn, int ken, int tariffIndex, long keyNo, int seqNo) {
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex);
  }

  public boolean areKeyChangeTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, String newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidKeyExpiredTime(newKen) &&
            isValidKeyVersionNumber(newKrn) &&
            isValidSgc(newSgc) &&
            isValidKeyNo(newKeyNo) &&
            isValidTariffIndex(newTariffIndex);
  }

  public boolean areMaxPowerLimitTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, String activatingDate, int activationModel, int[] maxPowerLimits, int[] hours){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidDate(activatingDate) &&
            isValidActivatingModel(activationModel) &&
            isValidMaxPowerLimit(maxPowerLimits) &&
            isValidHour(hours) &&
            (hours.length == maxPowerLimits.length);
  }

  public boolean areTouTariffTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo,
                                                int seqNo,  int activatingModel, int validate, String activatingDate,
                                                int[] rates, int[] times){

    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidDate(activatingDate) &&
            isValidActivatingModel(activatingModel) &&
            isValidValidate(validate) &&
            isValidTimes(times) &&
            isValidRates(rates) &&
            rates.length == times.length;
  }

  public boolean areSingleTariffTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken,
                                                     long keyNo, int seqNo, String activatingDate, int activatingModel,
                                                     int validate, int rate){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidDate(activatingDate) &&
            isValidActivatingModel(activatingModel) &&
            isValidValidate(validate) &&
            isValidSingleTariffRate(rate);
  }

  public boolean areStepTariffTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo,
                                                   int seqNo, String activatingDate, int activatingModel, int validate,
                                                   int[] rates, int[] steps, int [] flag){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidDate(activatingDate) &&
            isValidActivatingModel(activatingModel) &&
            isValidValidate(validate) &&
            isValidRates(rates) &&
            isValidSteps(steps) &&
            isValidFlag(flag);

  }


  public boolean areHolidayModeTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken,
                                                    long keyNo, int seqNo, int holidayMode, String[] days){

    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidHolidayMode(holidayMode) &&
            isValidHolidays(days);
  }


  public boolean areFriendModeTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo,
                                                   int seqNo, int friendMode, int[] hours, int[] days, int numOfAllowbaleDays){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidFriendMode(friendMode) &&
            isValidFriendModeHours(hours) &&
            isValidFriendModeDays(days) &&
            isValidNumOfAllowableDays(numOfAllowbaleDays);
  }


  public boolean areChangeMeterModeTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken,
                                                        long keyNo, int seqNo, int mode){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo) &&
            isValidTariffIndex(tariffIndex) &&
            isValidMeterMode(mode);
  }

  public boolean areLogoffReturnTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken,long keyNo, int seqNo){
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidTariffIndex(tariffIndex) &&
            isValidKeyNo(keyNo) &&
            isValidSeqNo(seqNo);
  }

  public boolean areMeterTestTokenParametersValid(int manufacturerID, int control){
    return isValidManufacturerID(manufacturerID) &&
            isValidControl(control);
  }

  public boolean AreResolveReturnTokenParametersValid(String meterNo, String sgc, int tariffIndex, int krn, int ken,
                                                      long keyNo, String[] tokens)
  {
    return isValidKeyExpiredTime(ken) &&
            isValidKeyVersionNumber(krn) &&
            isValidMeterNo(meterNo) &&
            isValidSgc(sgc) &&
            isValidTariffIndex(tariffIndex) &&
            isValidKeyNo(keyNo) &&
            isValidReturnTokens(tokens);
  }

  private boolean isValidReturnTokens(String[] tokens)
  {
    if (tokens == null)
      return false;
    return tokens.length == 2 && tokens[0].length() == 20 && tokens[1].length() == 20;
  }
}
