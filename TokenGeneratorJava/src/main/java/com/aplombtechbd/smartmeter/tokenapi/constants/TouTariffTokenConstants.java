package com.aplombtechbd.smartmeter.tokenapi.constants;

public class TouTariffTokenConstants {

  public static final int TouTariffPadTotalBits = 6;
  public static final int TouTariffHourTotalBits = 5;
  public static final int TouTariffRateTotalBits = 16;
  public static final int TouTariffActiveModelTotalBits = 1;
  public static final int TouTariffValidateTotalBits = 3;
  public static final String TouTariffValidateValue = "0";

  public static final String TouTariffRateSettingClass = "2";
  public static final String TouTariffRateSettingSubClass = "9";

  public static final String TouTariffActiveModeClass = "2";
  public static final String TouTariffActiveModeSubClass = "10";
  public static final int TouTariffRatesLengthTotalBits = 6;
}
