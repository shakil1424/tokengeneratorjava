package com.aplombtechbd.smartmeter.tokenapi;

import com.aplombtechbd.smartmeter.tokenapi.helper.*;
import com.aplombtechbd.smartmeter.tokenapi.validation.Validator;
import com.aplombtechbd.smartmeter.tokenapi.constants.*;

public class TokenImplementation implements AplTokenApi{
  private final int INVALID_DATA_ERROR_CODE = -1;
  private CreditTokenGenerationHelper creditTokenGenerationHelper;
  private TokenGenerationHelper tokenGenerationHelper;
  private DecoderKeyGeneration decoderKeyGeneration;
  private ClearEventTokenGenerationHelper clearEventTokenGenerationHelper;
  private ClearBalanceTokenGenerationHelper clearBalanceTokenGenerationHelper;
  private Validator validator;
  private ByteArrayUtility byteArrayUtility;
  private KeyChangeTokenGenerationHelper keyChangeTokenGenerationHelper;
  private MaxPowerLimitTokenGenerationHelper maxPowerLimitTokenGenerationHelper;
  private TouTariffTokenGenerationHelper touTariffTokenGenerationHelper;
  private SingleTariffTokenGenerationHelper singleTariffTokenGenerationHelper;
  private StepTariffTokenGenerationHelper stepTariffTokenGenerationHelper;
  private HolidayModeTokenGenerationHelper holidayModeTokenGenerationHelper;
  private FriendModeTokenGenerationHelper friendModeTokenGenerationHelper;
  private ChangeMeterModeTokenGenerationHelper changeMeterModeTokenGenerationHelper;
  private LogoffReturnTokenGenerationHelper logoffReturnTokenGenerationHelper;
  private MeterTestTokenGenerationHelper meterTestTokenGenerationHelper;
  private String token20digitString;
  private byte[] decoderKey;
  public static int StepTariffFlagErrorCode = 0;

  public TokenImplementation() {
    tokenGenerationHelper = new TokenGenerationHelper();
    decoderKeyGeneration = new DecoderKeyGeneration();
    creditTokenGenerationHelper = new CreditTokenGenerationHelper();
    clearEventTokenGenerationHelper = new ClearEventTokenGenerationHelper();
    clearBalanceTokenGenerationHelper = new ClearBalanceTokenGenerationHelper();
    validator = new Validator();
    byteArrayUtility = new ByteArrayUtility();
    keyChangeTokenGenerationHelper = new KeyChangeTokenGenerationHelper();
    maxPowerLimitTokenGenerationHelper = new MaxPowerLimitTokenGenerationHelper();
    touTariffTokenGenerationHelper = new TouTariffTokenGenerationHelper();
    singleTariffTokenGenerationHelper = new SingleTariffTokenGenerationHelper();
    stepTariffTokenGenerationHelper = new StepTariffTokenGenerationHelper();
    holidayModeTokenGenerationHelper = new HolidayModeTokenGenerationHelper();
    friendModeTokenGenerationHelper = new FriendModeTokenGenerationHelper();
    changeMeterModeTokenGenerationHelper = new ChangeMeterModeTokenGenerationHelper();
    logoffReturnTokenGenerationHelper = new LogoffReturnTokenGenerationHelper();
    meterTestTokenGenerationHelper  = new MeterTestTokenGenerationHelper();
  }

  public String getCreditToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int amount) {

    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (tokenGenerationHelper.hasDecimalPoint(amount)) {
      amount = amount * 100;
    }
    if (validator.areCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, (long) amount)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(CreditTokenConstants.creditClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(CreditTokenConstants.creditSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] amountBytes = byteArrayUtility.formatByteArray(String.valueOf((long) amount), CreditTokenConstants.creditTokenAmountTotalBits);
      byte[] dataBlock64bit = creditTokenGenerationHelper.getCreditTokenDataBlock(ClassBytes, subClassBytes, seqNoBytes, amountBytes);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getSetLowCreditWarningLimitToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int amountType, int amountLimit) {

    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (tokenGenerationHelper.hasDecimalPoint(amountLimit)) {
      amountLimit = amountLimit * 100;
    }

    if (validator.areCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, (long) amountLimit)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(LowCreditWarnigConstants.lowCreditClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(LowCreditWarnigConstants.lowCreditSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] amountBytes = byteArrayUtility.formatByteArray(String.valueOf((long) amountLimit), LowCreditWarnigConstants.lowCreditAmountTotalBits);
      byte[] dataBlock64bit = creditTokenGenerationHelper.getCreditTokenDataBlock(ClassBytes, subClassBytes, seqNoBytes, amountBytes);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }


  public String SetLowCreditWarningLimitToken(String meterNo, String Sgc, int tarrifIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amountType, int amountLimit) {
    return getSetLowCreditWarningLimitToken(meterNo, Sgc, tarrifIndex, keyVersion, keyExpiredTime, keyNo, seqNo, amountType, amountLimit);
  }


  public String getSetCreditAmountLimitOrOverdrawAmountLimitToken(String meterNo, String Sgc, int tarriffIndex, int krn, int ken, long keyNo, int seqNo, int amountType, int amountLimit) {
    if(amountType == 0)
       return generateSetCreditAmountLimitToken(meterNo, Sgc, tarriffIndex, krn, ken, keyNo, seqNo, amountLimit);
    if(amountType == 1)
      return generateSetOverdrawAmountLimitToken(meterNo, Sgc, tarriffIndex, krn, ken, keyNo, seqNo, amountLimit);
    return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
  }

  public String generateSetCreditAmountLimitToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, double amountLimit) {
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (tokenGenerationHelper.hasDecimalPoint(amountLimit)) {
      amountLimit = amountLimit * 100;
    }

    if (validator.areCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, (long) amountLimit)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(CreditAmoutLimitConstants.creditAmountLimitClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(CreditAmoutLimitConstants.creditAmountLimitSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] amountBytes = byteArrayUtility.formatByteArray(String.valueOf((long) amountLimit), CreditAmoutLimitConstants.creditAmountLimitAmountTotalBits);
      byte[] dataBlock64bit = creditTokenGenerationHelper.getCreditTokenDataBlock(ClassBytes, subClassBytes, seqNoBytes, amountBytes);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String generateSetOverdrawAmountLimitToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, double amountLimit) {
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (tokenGenerationHelper.hasDecimalPoint(amountLimit)) {
      amountLimit = amountLimit * 100;
    }

    if (validator.areCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, (long) amountLimit)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(OverdrawAmountLimitConstants.overdrawAmountClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(OverdrawAmountLimitConstants.overdrawAmountSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] amountBytes = byteArrayUtility.formatByteArray(String.valueOf((long) amountLimit), OverdrawAmountLimitConstants.overdrawAmountLimitAmountTotalBits);
      byte[] dataBlock64bit = creditTokenGenerationHelper.getCreditTokenDataBlock(ClassBytes, subClassBytes, seqNoBytes, amountBytes);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getClearEventToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo) {
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (validator.areClearTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(ClearEventTokenConstants.clearEventClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(ClearEventTokenConstants.clearEventSubClass, CommonConstants.SubClassTotalBits);
      String randomNumber = String.valueOf(tokenGenerationHelper.getFourBitRandomNumber());
      byte[] rnd = byteArrayUtility.formatByteArray(randomNumber, ClearEventTokenConstants.clearEventRndTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, ClearEventTokenConstants.clearEventPadTotalBits);
      byte[] dataBlock64bit = clearEventTokenGenerationHelper.getClearEventTokenDataBlock(ClassBytes, subClassBytes, rnd, seqNoBytes, pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getClearBalanceToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo) {
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String meterNo_s = String.valueOf(meterNo);

    if (validator.areClearTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo_s);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(ClearBalanceTokenConstants.clearBalanceClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(ClearBalanceTokenConstants.clearBalanceSubClass, CommonConstants.SubClassTotalBits);
      String randomNumber = String.valueOf(tokenGenerationHelper.getFourBitRandomNumber());
      byte[] rnd = byteArrayUtility.formatByteArray(randomNumber, ClearBalanceTokenConstants.clearBalanceRndTotalBits);
      byte[] register = byteArrayUtility.formatByteArray(CommonConstants.PadValue, ClearBalanceTokenConstants.clearBalanceRegisterTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, ClearBalanceTokenConstants.clearBalancePadTotalBits);
      byte[] dataBlock64bit = clearBalanceTokenGenerationHelper.getClearBalanceTokenDataBlock(ClassBytes, subClassBytes, rnd, seqNoBytes, register, pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getKeyChangeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, String newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo) {
    String keyType = CommonConstants.kt;
    String newTariffIndex_s = String.valueOf(newTariffIndex);
    String newKrn_S = String.valueOf(newKrn);
    String meterNo_s = String.valueOf(meterNo);
    if (validator.areKeyChangeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, newSgc, newTariffIndex, newKrn, newKen, newKeyNo)) {

      byte[] decoderKeyOld = decoderKeyGeneration.getDecoderKey(keyType, sgc, String.valueOf(tariffIndex), String.valueOf(krn), meterNo);
      byte[] decoderKeyNew = decoderKeyGeneration.getDecoderKey(keyType, newSgc, newTariffIndex_s, newKrn_S, meterNo_s);
      this.decoderKey = decoderKeyOld;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.token1Class, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.token1SubClass, CommonConstants.SubClassTotalBits);
      byte[] KENHO = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.KENHO_value, KeyChangeTokenConstants.KENHO_totalBits);
      byte[] KRN = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.KRN_value, KeyChangeTokenConstants.KRN_totalBits);
      byte[] RO = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.RO_value_1, KeyChangeTokenConstants.RO_totalBits);
      byte[] Res = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.Res_value, KeyChangeTokenConstants.Res_totalBits);
      byte[] KT = byteArrayUtility.formatByteArray(keyType, KeyChangeTokenConstants.KT_totalBits);
      byte[] NKHO = keyChangeTokenGenerationHelper.getNKHO(decoderKeyNew);
      byte[] dataBlock64bitToken1 = keyChangeTokenGenerationHelper.getKeyChangeTokenDataBlock(ClassBytes, subClassBytes, KENHO, KRN, RO, Res, KT, NKHO);
      byte[] eightByteDataBlockToken1 = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bitToken1);
      byte[] encryptedDataBlockToken1 = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlockToken1, decoderKeyOld);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlockToken1);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[2];
      tokens[0] = twentyDigitToken;

      byte[] ClassBytes2 = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.token2Class, CommonConstants.ClassTotalBits);
      byte[] subClassBytes2 = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.token2SubClass, CommonConstants.SubClassTotalBits);
      byte[] KENLO = byteArrayUtility.formatByteArray(KeyChangeTokenConstants.KENLO_value, KeyChangeTokenConstants.KENLO_totalBits);
      byte[] TI = byteArrayUtility.formatByteArray(String.valueOf(newTariffIndex), KeyChangeTokenConstants.TI_totalBits);
      byte[] NKLO = keyChangeTokenGenerationHelper.getNKLO(decoderKeyNew);
      byte[] dataBlock64bitToken2 = keyChangeTokenGenerationHelper.getKeyChangeTokenDataBlockToken2(ClassBytes2, subClassBytes2, KENLO, TI, NKLO);
      byte[] eightByteDataBlockToken2 = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bitToken2);
      byte[] encryptedDataBlockToken2 = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlockToken2, decoderKeyOld);
      String encrypted64BitBinaryStringToken2 = byteArrayUtility.getPaddedBinaryString(encryptedDataBlockToken2);
      String token66bitsBinaryStringToken2 = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryStringToken2, ClassBytes2);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryStringToken2);
      this.setToken20digitString(twentyDigitToken);
      tokens[1] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getKeyChangeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, String newSgc, int newTariffIndex, int newKrn, int newKen, int newKeyNo) {
    return getKeyChangeToken(meterNo, sgc, tariffIndex, krn, ken, keyNo, newSgc, newTariffIndex, newKrn, newKen, (long)newKeyNo);
  }

  public String getMaxPowerLimitToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int activationModel, String activatingDate, int[] maxPowerLimits, int[] hours) {
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);

    if (validator.areMaxPowerLimitTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, activatingDate,
            activationModel, maxPowerLimits, hours)) {

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(MaxPowerLimitTokenConstants.maxPowerLimitClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(MaxPowerLimitTokenConstants.maxPowerLimitSubClass, CommonConstants.SubClassTotalBits);
      byte[] Pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, MaxPowerLimitTokenConstants.maxPowerLimitPadTotalBits);
      String[] tokens = new String[maxPowerLimits.length + 1];
      String twentyDigitToken;

      for(int i = 0; i < maxPowerLimits.length; i++){
        byte[] MPL = byteArrayUtility.formatByteArray(String.valueOf(maxPowerLimits[i]), MaxPowerLimitTokenConstants.maxPowerLimitMPLtotalBits);
        byte[] Hour = byteArrayUtility.formatByteArray(String.valueOf(hours[i]), MaxPowerLimitTokenConstants.maxPowerLimitHourTotalBits);
        long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
        byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
        byte[] dataBlock64bit = maxPowerLimitTokenGenerationHelper.getMaxPowerLimitDataBlock(ClassBytes, subClassBytes, seqNoBytes, Hour, MPL, Pad);
        byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
        byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
        String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
        String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

        twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
        this.setToken20digitString(twentyDigitToken);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes = byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] classActiveMode = byteArrayUtility.formatByteArray(MaxPowerLimitTokenConstants.maxPowerLimitActiveModeClass, CommonConstants.ClassTotalBits);
      byte[] subclassActiveMode = byteArrayUtility.formatByteArray(MaxPowerLimitTokenConstants.maxPowerLimitActiveModeSubClass, CommonConstants.SubClassTotalBits);
      byte[] activeModelbytes = byteArrayUtility.formatByteArray(String.valueOf(activationModel), MaxPowerLimitTokenConstants.maxPowerLimitActiveModelTotalBits);
      String year_s = activatingDate.substring(2,4);
      String month_s = activatingDate.substring(5, 7);
      String day_s = activatingDate.substring(8, 10);
      byte[] yearBytes = byteArrayUtility.formatByteArray(year_s, CommonConstants.YearTotalBits);
      byte[] monthBytes = byteArrayUtility.formatByteArray(month_s, CommonConstants.MonthTotalBits);
      byte[] dayBytes = byteArrayUtility.formatByteArray(day_s, CommonConstants.DayTotalBits);
      byte[] maxPowerLimitsLength = byteArrayUtility.formatByteArray(String.valueOf(maxPowerLimits.length), MaxPowerLimitTokenConstants.MaxPowerLimitsLengthTotalBits);
      byte[] dataBlock64bit = maxPowerLimitTokenGenerationHelper.getMaxPowerLimitActiveModeDataBlock(classActiveMode, subclassActiveMode, seqNoBytes, activeModelbytes,yearBytes, monthBytes, dayBytes, maxPowerLimitsLength);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[maxPowerLimits.length] = twentyDigitToken;
      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getTOUTariffToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, String activatingDate, int activatingModel, int validate, int[] rates, int[] times){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);

    if(validator.areTouTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, activatingModel,
            validate, activatingDate, rates, times)){

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(TouTariffTokenConstants.TouTariffRateSettingClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(TouTariffTokenConstants.TouTariffRateSettingSubClass, CommonConstants.SubClassTotalBits);
      byte[] Pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, TouTariffTokenConstants.TouTariffPadTotalBits);
      String[] tokens = new String[rates.length + 1];
      String twentyDigitToken;

      for(int i = 0; i < rates.length; i++){
        long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
        byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
        byte[] Hour = byteArrayUtility.formatByteArray(String.valueOf(times[i]), TouTariffTokenConstants.TouTariffHourTotalBits);
        byte[] Rate = byteArrayUtility.formatByteArray(String.valueOf(rates[i]), TouTariffTokenConstants.TouTariffRateTotalBits);
        byte[] dataBlock64bit = touTariffTokenGenerationHelper.getTouTariffRateSettingDataBlock(ClassBytes, subClassBytes, seqNoBytes, Hour, Rate, Pad);
        byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
        byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
        String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
        String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

        twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
        this.setToken20digitString(twentyDigitToken);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] classActiveMode = byteArrayUtility.formatByteArray(TouTariffTokenConstants.TouTariffActiveModeClass, CommonConstants.ClassTotalBits);
      byte[] subclassActiveMode = byteArrayUtility.formatByteArray(TouTariffTokenConstants.TouTariffActiveModeSubClass, CommonConstants.SubClassTotalBits);
      byte[] activeModelBytes = byteArrayUtility.formatByteArray(String.valueOf(activatingModel), TouTariffTokenConstants.TouTariffActiveModelTotalBits);
      byte[] validateBytes = byteArrayUtility.formatByteArray(String.valueOf(validate), TouTariffTokenConstants.TouTariffValidateTotalBits);
      String year_s = activatingDate.substring(2,4);
      String month_s = activatingDate.substring(5, 7);
      String day_s = activatingDate.substring(8, 10);
      byte[] yearBytes = byteArrayUtility.formatByteArray(year_s, CommonConstants.YearTotalBits);
      byte[] monthBytes = byteArrayUtility.formatByteArray(month_s, CommonConstants.MonthTotalBits);
      byte[] dayBytes = byteArrayUtility.formatByteArray(day_s, CommonConstants.DayTotalBits);
      byte[] ratesLength = byteArrayUtility.formatByteArray(String.valueOf(rates.length), TouTariffTokenConstants.TouTariffRatesLengthTotalBits);

      byte[] dataBlock64bit = touTariffTokenGenerationHelper.getTouTariffActiveModeDataBlock(classActiveMode, subclassActiveMode, seqNoBytes, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, ratesLength);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[rates.length] = twentyDigitToken;
      return tokenGenerationHelper.generateTokenXml(0, tokens);

    }else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getSingleTariffToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, String activatingDate, int activatingModel, int validate, int rate){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String[] tokens = new String[2];
    String twentyDigitToken;

    if(validator.areSingleTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, activatingDate, activatingModel, validate, rate)){

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(SingleTariffTokenConstants.SingleTariffClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(SingleTariffTokenConstants.SingleTariffSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), SingleTariffTokenConstants.SingleTariffSeqNoTotalBits);
      byte[] Rate = byteArrayUtility.formatByteArray(String.valueOf(rate), SingleTariffTokenConstants.SingleTariffRateTotalBits);
      byte[] Pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, SingleTariffTokenConstants.SingleTariffRateSettingsPadTotalBits);

      byte[] dataBlock64bit = singleTariffTokenGenerationHelper.getSingleTariffRateSettingDataBlock(ClassBytes, subClassBytes, seqNoBytes, Rate, Pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[0] = twentyDigitToken;

      seqNo++;

      seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), SingleTariffTokenConstants.SingleTariffSeqNoTotalBits);
      byte[] classActiveMode = byteArrayUtility.formatByteArray(SingleTariffTokenConstants.SingleTariffActiveModeClass, CommonConstants.ClassTotalBits);
      byte[] subclassActiveMode = byteArrayUtility.formatByteArray(SingleTariffTokenConstants.SingleTariffActiveModeSubClass, CommonConstants.SubClassTotalBits);
      byte[] activeModelBytes = byteArrayUtility.formatByteArray(String.valueOf(activatingModel), SingleTariffTokenConstants.SingleTariffActiveModelTotalBits);
      byte[] validateBytes = byteArrayUtility.formatByteArray(String.valueOf(validate), SingleTariffTokenConstants.SingleTariffValidateTotalBits);
      String year_s = activatingDate.substring(2,4);
      String month_s = activatingDate.substring(5, 7);
      String day_s = activatingDate.substring(8, 10);
      byte[] yearBytes = byteArrayUtility.formatByteArray(year_s, CommonConstants.YearTotalBits);
      byte[] monthBytes = byteArrayUtility.formatByteArray(month_s, CommonConstants.MonthTotalBits);
      byte[] dayBytes = byteArrayUtility.formatByteArray(day_s, CommonConstants.DayTotalBits);
      byte[] padActiveMode = byteArrayUtility.formatByteArray(CommonConstants.PadValue, SingleTariffTokenConstants.SingleTariffActiveModePadTotalBits);

      dataBlock64bit = singleTariffTokenGenerationHelper.getSingleTariffActiveModeDataBlock(classActiveMode, subclassActiveMode, seqNoBytes, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, padActiveMode);
      eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[1] = twentyDigitToken;
      return tokenGenerationHelper.generateTokenXml(0, tokens);

    }else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }


  public String getStepTariffToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, String activatingDate, int validate, int activatingModel, int[] rates, int[] steps, int [] flag){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String twentyDigitToken;

    if(validator.areStepTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, activatingDate,
            activatingModel, validate, rates, steps, flag)){
      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffRateClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffRateSubClass, CommonConstants.SubClassTotalBits);
      String[] tokens = new String[rates.length + 2];

      for (int i = 0; i < rates.length; i++){
        int [] stepsArray = new int[rates.length];
        if (steps.length == rates.length - 1){
          System.arraycopy(steps, 0, stepsArray, 1, steps.length);
          stepsArray[0] = 0;
        } else {
          stepsArray = new int[steps.length];
          stepsArray = steps;
        }

        long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
        byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
        byte[] Step = byteArrayUtility.formatByteArray(String.valueOf(stepsArray[i]), StepTariffTokenConstants.StepTariffStepTotalBits);
        byte[] Rate = byteArrayUtility.formatByteArray(String.valueOf(rates[i]), StepTariffTokenConstants.StepTariffRateTotalBits);
        byte[] Pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, StepTariffTokenConstants.StepTariffRatePadTotalBits );
        byte[] dataBlock64bit = stepTariffTokenGenerationHelper.getStepTariffRateSettingsDataBlock(ClassBytes, subClassBytes, seqNoBytes, Step, Rate, Pad);
        byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
        byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
        String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
        String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

        twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
        this.setToken20digitString(twentyDigitToken);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      ClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffActiveClass, CommonConstants.ClassTotalBits);
      subClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffActiveSubClass, CommonConstants.SubClassTotalBits);
      byte[] activeModelBytes = byteArrayUtility.formatByteArray(String.valueOf(activatingModel), StepTariffTokenConstants.StepTariffActiveModelTotalBits);
      byte[] validateBytes = byteArrayUtility.formatByteArray(String.valueOf(validate), StepTariffTokenConstants.StepTariffValidateTotalBits);
      String year_s = activatingDate.substring(2,4);
      String month_s = activatingDate.substring(5, 7);
      String day_s = activatingDate.substring(8, 10);
      byte[] yearBytes = byteArrayUtility.formatByteArray(year_s, CommonConstants.YearTotalBits);
      byte[] monthBytes = byteArrayUtility.formatByteArray(month_s, CommonConstants.MonthTotalBits);
      byte[] dayBytes = byteArrayUtility.formatByteArray(day_s, CommonConstants.DayTotalBits);
      byte[] lengthOfRates = byteArrayUtility.formatByteArray(String.valueOf(rates.length), StepTariffTokenConstants.StepTariffActiveRatesLengthTotalBits);
      byte[] dataBlock64bit = stepTariffTokenGenerationHelper.getStepTariffActiveModeDataBlock(ClassBytes, subClassBytes, seqNoBytes, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, lengthOfRates);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[rates.length] = twentyDigitToken;

      seqNo++;

      seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      ClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffFlagClass, CommonConstants.ClassTotalBits);
      subClassBytes = byteArrayUtility.formatByteArray(StepTariffTokenConstants.StepTariffFlagSubClass, CommonConstants.SubClassTotalBits);
      byte[] Flag = stepTariffTokenGenerationHelper.getFlagBytes(flag);
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, StepTariffTokenConstants.StepTariffFlagPadTotalBits);
      dataBlock64bit = stepTariffTokenGenerationHelper.getStepTariffFlagSettingsDataBlock(ClassBytes, subClassBytes, seqNoBytes, Flag, pad);
      eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[rates.length+1] = twentyDigitToken;
      return tokenGenerationHelper.generateTokenXml(0, tokens);

    }
    else {
      if(StepTariffFlagErrorCode == 0)
        return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
      else
        return tokenGenerationHelper.generateTokenXml(StepTariffFlagErrorCode, (String[])null);
    }

  }


  public String getHolidayModeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, int keyNo, int seqNo, int holidayMode, String[] days){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String[] tokens = new String[days.length];
    String twentyDigitToken;

    if(validator.areHolidayModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, holidayMode, days)){

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(HolidayModeTokenConstants.holidayModeClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(HolidayModeTokenConstants.holidayModeSubClass, CommonConstants.SubClassTotalBits);
      byte[] holidayModeBytes = byteArrayUtility.formatByteArray(String.valueOf(holidayMode),HolidayModeTokenConstants.holidayModeTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, HolidayModeTokenConstants.holidayModePadTotalBits);

      for (int i = 0; i < days.length; i++){
        String randomNumber = String.valueOf(tokenGenerationHelper.getFourBitRandomNumber());
        byte[] rnd = byteArrayUtility.formatByteArray(randomNumber, HolidayModeTokenConstants.holidayModeRndTotalBits);
        String year_s = days[i].substring(2,4);
        String month_s = days[i].substring(5, 7);
        String day_s = days[i].substring(8, 10);
        byte[] yearBytes = byteArrayUtility.formatByteArray(year_s, CommonConstants.YearTotalBits);
        byte[] monthBytes = byteArrayUtility.formatByteArray(month_s, CommonConstants.MonthTotalBits);
        byte[] dayBytes = byteArrayUtility.formatByteArray(day_s, CommonConstants.DayTotalBits);
        long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
        byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
        byte[] dataBlock64bit = holidayModeTokenGenerationHelper.getHolidayModeDataBlock(ClassBytes, subClassBytes, rnd,
                seqNoBytes, holidayModeBytes, yearBytes, monthBytes, dayBytes, pad);
        byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
        byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
        String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
        String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

        twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
        this.setToken20digitString(twentyDigitToken);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }
      return tokenGenerationHelper.generateTokenXml(0, tokens);

    }else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }

  }

  public String generateHolidayModeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, int keyNo, int seqNo, int holidayMode, String[] days) {
    return getHolidayModeToken(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, holidayMode, days);
  }


  public String getToken20digitString() {
    return token20digitString;
  }

  public void setToken20digitString(String token20digitString) {
    this.token20digitString = token20digitString;
  }

  public byte[] getDecoderKey() {
    return decoderKey;
  }


  public String getFriendModeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int friendMode, int[] hours, int[] days, int numberOfAllowableDays){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    String[] tokens = new String[1];
    String twentyDigitToken;

    if(validator.areFriendModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, friendMode, hours,
            days, numberOfAllowableDays)){

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(FriendModeTokenConstants.FriendModeClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(FriendModeTokenConstants.FriendModeSubClass, CommonConstants.SubClassTotalBits);
      byte[] friendModeBytes = byteArrayUtility.formatByteArray(String.valueOf(friendMode), FriendModeTokenConstants.FriendModeTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, FriendModeTokenConstants.FriendModePadlBits);
      byte[] allowableDaysBytes = byteArrayUtility.formatByteArray(String.valueOf(numberOfAllowableDays), FriendModeTokenConstants.FriendModeAllowableDaysTotalBits);

      byte[] hourStart = byteArrayUtility.formatByteArray(String.valueOf(hours[0]), FriendModeTokenConstants.FriendModeHourStartTotalBits);
      byte[] hourEnd = byteArrayUtility.formatByteArray(String.valueOf(hours[1]), FriendModeTokenConstants.FriendModeHourEndTotalBits);
      byte[] weekBytes = friendModeTokenGenerationHelper.getWeekDaysBytes(days);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] dataBlock64bit = friendModeTokenGenerationHelper.getFriendModeDataBlock(ClassBytes, subClassBytes,
                seqNoBytes, friendModeBytes, hourStart, hourEnd, weekBytes, allowableDaysBytes, pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }


  public String getChangeMeterModeToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int mode){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);

    if(validator.areChangeMeterModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, mode)){

      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;
      byte[] ClassBytes = byteArrayUtility.formatByteArray(ChangeMeterModeTokenConstants.ChangeMeterModeClass,CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(ChangeMeterModeTokenConstants.ChangeMeterModeSubClass, CommonConstants.SubClassTotalBits);
      String randomNumber = String.valueOf(tokenGenerationHelper.getFourBitRandomNumber());
      byte[] rnd = byteArrayUtility.formatByteArray(randomNumber, ChangeMeterModeTokenConstants.ChangeMeterModeRndTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] modeBytes = byteArrayUtility.formatByteArray(String.valueOf(mode),ChangeMeterModeTokenConstants.ChangeMeterModeTotalBits );
      byte[] pad = byteArrayUtility.formatByteArray(CommonConstants.PadValue, ChangeMeterModeTokenConstants.ChangeMeterModePadTotalBits);
      byte[] dataBlock64bit = changeMeterModeTokenGenerationHelper.getChangeMeterModeTokenDataBlock(ClassBytes, subClassBytes, rnd, seqNoBytes, modeBytes, pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);
    }else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String getLogoffReturnToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo){
    String keyType = CommonConstants.kt;
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);

    if(validator.areLogoffReturnTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo)) {
      byte[] decoderKey64bit = decoderKeyGeneration.getDecoderKey(keyType, sgc, tariffIndex_s, krn_s, meterNo);
      this.decoderKey = decoderKey64bit;

      byte[] ClassBytes = byteArrayUtility.formatByteArray(LogoffReturnTokenConstants.LogoffReturnClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(LogoffReturnTokenConstants.LogoffReturnSubClass, CommonConstants.SubClassTotalBits);
      long seqNo_L = tokenGenerationHelper.getSeqNo(keyNo, seqNo);
      byte[] seqNoBytes =  byteArrayUtility.formatByteArray(String.valueOf(seqNo_L), CommonConstants.SeqNoTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray(LogoffReturnTokenConstants.LogoffReturnPadValue, LogoffReturnTokenConstants.LogoffReturnPadTotalBits);
      byte[] dataBlock64bit = logoffReturnTokenGenerationHelper.getLogoffReturnTokenDataBlock(ClassBytes, subClassBytes, seqNoBytes, pad);
      byte[] eightByteDataBlock = byteArrayUtility.convert64bitDataBlockTo8ByteDataBlock(dataBlock64bit);
      byte[] encryptedDataBlock = tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64bit);
      String encrypted64BitBinaryString = byteArrayUtility.getPaddedBinaryString(encryptedDataBlock);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(encrypted64BitBinaryString, ClassBytes);

      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;

      return tokenGenerationHelper.generateTokenXml(0, tokens);

    } else {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }


  public String generateLogoffReturnToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo) {
    return getLogoffReturnToken(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo);
  }

  public String getTestToken(int manufacturerID, int control) {
    if(validator.areMeterTestTokenParametersValid(manufacturerID, control)) {
      byte[] ClassBytes = byteArrayUtility.formatByteArray(MeterTestTokenConstants.MeterTestClass, CommonConstants.ClassTotalBits);
      byte[] subClassBytes = byteArrayUtility.formatByteArray(MeterTestTokenConstants.MeterTestSubClass, CommonConstants.SubClassTotalBits);
      byte[] manufacturerIdBytes = byteArrayUtility.formatByteArray(String.valueOf(manufacturerID), MeterTestTokenConstants.ManufacturerIdTotalBits);
      byte[] controlBytes = byteArrayUtility.formatByteArray(String.valueOf(control), MeterTestTokenConstants.ControlTotalBits);
      byte[] pad = byteArrayUtility.formatByteArray("0", MeterTestTokenConstants.PadTotalBits);

      byte[] dataBlock64bit = meterTestTokenGenerationHelper.getMeterTestTokenDataBlock(ClassBytes, subClassBytes, manufacturerIdBytes, controlBytes, pad);
      String dataBlock64bitBinaryString = byteArrayUtility.getBinaryString(dataBlock64bit);
      String token66bitsBinaryString = tokenGenerationHelper.transposeClassBits(dataBlock64bitBinaryString, ClassBytes);
      String twentyDigitToken = tokenGenerationHelper.getTokenBigInteger(token66bitsBinaryString);
      this.setToken20digitString(twentyDigitToken);
      String[] tokens = new String[1];
      tokens[0] = twentyDigitToken;
      return tokenGenerationHelper.generateTokenXml(0, tokens);
    }
    else{
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }
  }

  public String generateSwitchModeN2PToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo, int mode) {
    return getChangeMeterModeToken(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, mode);
  }

  public String resolveReturnToken(String meterNo, String sgc, int tariffIndex, int krn, int ken, long keyNo, String token) {
    String[] tokens = new String[2];
    tokens[0] = token.substring(0, 20);
    tokens[1] = token.substring(20, 40);
    String tariffIndex_s = String.valueOf(tariffIndex);
    String krn_s = String.valueOf(krn);
    if (!validator.AreResolveReturnTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, tokens))
    {
      return tokenGenerationHelper.generateTokenXml(INVALID_DATA_ERROR_CODE, (String[])null);
    }

    String token1 = tokens[0];
    String token2 = tokens[1];
    ResolveReturnTokenGenerationHelper resolveReturnTokenGenerationHelper = new ResolveReturnTokenGenerationHelper();
    byte[] decoderKey =
            decoderKeyGeneration.getDecoderKey(CommonConstants.kt, sgc, tariffIndex_s, krn_s, meterNo);
    byte[] firstTokenDataBlock = resolveReturnTokenGenerationHelper.getReturnTokenDecryptedDataBlock(token1, decoderKey);
    byte[] secondTokenDataBlock = resolveReturnTokenGenerationHelper.getReturnTokenDecryptedDataBlock(token2, decoderKey);

    ResolveReturnFirstTokenData firstTokenData = resolveReturnTokenGenerationHelper.getFirstTokenData(firstTokenDataBlock);
    ResolveReturnSecondTokenData secondTokenData = resolveReturnTokenGenerationHelper.getSecondTokenData(secondTokenDataBlock);

    return tokenGenerationHelper.generateResolveReturnTokenXml(firstTokenData, secondTokenData, tariffIndex);
  }
}