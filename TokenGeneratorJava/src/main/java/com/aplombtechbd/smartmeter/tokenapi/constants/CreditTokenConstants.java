package com.aplombtechbd.smartmeter.tokenapi.constants;

public class CreditTokenConstants {
  public static final int creditTokenAmountTotalBits = 27;
  public static final String creditClass = "0";
  public static final String creditSubClass = "4";
}
