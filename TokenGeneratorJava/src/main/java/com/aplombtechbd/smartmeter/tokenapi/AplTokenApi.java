package com.aplombtechbd.smartmeter.tokenapi;

public interface AplTokenApi {
  String getCreditToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amount);

  String getSetLowCreditWarningLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amountType, int amountLimit);

  String getSetCreditAmountLimitOrOverdrawAmountLimitToken(String meterNo, String Sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amountType, int amountLimit);

  String getClearEventToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

  String getClearBalanceToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

  String getKeyChangeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, String newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo);

  String getMaxPowerLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int activationModel, String activatingDate, int[] maxPowerLimits, int[] hours);

  String getTOUTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, String activatingDate, int activatingModel, int validate, int[] rates, int[] times);

  String getSingleTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, String activatingDate, int activatingModel, int validate, int rate);

  String getStepTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, String activatingDate, int validate, int activatingModel, int[] rates, int[] steps, int[] flag);

  String getHolidayModeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int holidayMode, String[] days);

  String getFriendModeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int friendMode, int[] hours, int[] days, int numberOfAllowableDays);

  String generateSwitchModeN2PToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int mode);

  String getLogoffReturnToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

  String getTestToken(int manufacturerID, int control);

  String resolveReturnToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, String token);
}
