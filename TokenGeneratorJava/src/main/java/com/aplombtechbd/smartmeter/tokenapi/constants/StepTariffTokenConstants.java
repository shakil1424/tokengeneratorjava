package com.aplombtechbd.smartmeter.tokenapi.constants;

public class StepTariffTokenConstants {
  public static final int StepTariffStepTotalBits = 12;

  public static String StepTariffRateClass = "2";
  public static String StepTariffRateSubClass = "6";
  public static int StepTariffRatePadTotalBits = 2;
  public static final int StepTariffRateTotalBits = 13;

  public static String StepTariffActiveClass = "2";
  public static String StepTariffActiveSubClass = "7";
  public static int StepTariffActiveRatesLengthTotalBits = 6;
  public static final int StepTariffValidateTotalBits = 3;
  public static final int StepTariffActiveModelTotalBits = 1;

  public static String StepTariffFlagClass = "2";
  public static String StepTariffFlagSubClass = "8";
  public static int StepTariffFlagPadTotalBits = 19;
  public static int StepTariffFlagTotalBits = 8;
}
