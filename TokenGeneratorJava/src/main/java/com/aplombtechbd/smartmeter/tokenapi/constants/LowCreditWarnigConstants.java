package com.aplombtechbd.smartmeter.tokenapi.constants;

public class LowCreditWarnigConstants {
  public static final int lowCreditAmountTotalBits = 27;
  public static final String lowCreditClass = "2";
  public static final String lowCreditSubClass = "15";
}
