package com.aplombtechbd.smartmeter.tokenapi.constants;

public class SingleTariffTokenConstants {

  public static final int SingleTariffActiveModelTotalBits = 1;
  public static final int SingleTariffValidateTotalBits = 3;
  public static final String SingleTariffClass = "2";
  public static final String SingleTariffSubClass = "4";
  public static final int SingleTariffRateSettingsPadTotalBits = 14;
  public static final int SingleTariffRateTotalBits = 13;
  public static final int SingleTariffSeqNoTotalBits = 17;

  public static final String SingleTariffActiveModeClass = "2";
  public static final String SingleTariffActiveModeSubClass = "5";
  public static final int SingleTariffActiveModePadTotalBits = 6;
}
