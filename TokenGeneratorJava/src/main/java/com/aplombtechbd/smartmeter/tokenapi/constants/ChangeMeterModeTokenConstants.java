package com.aplombtechbd.smartmeter.tokenapi.constants;

public class ChangeMeterModeTokenConstants {
  public static final String ChangeMeterModeClass = "3";
  public static final String ChangeMeterModeSubClass = "2";
  public static final int ChangeMeterModePadTotalBits = 22;
  public static final int ChangeMeterModeTotalBits = 1;
  public static final int ChangeMeterModeRndTotalBits = 4;
}
