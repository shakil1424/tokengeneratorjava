package com.aplombtechbd.smartmeter.tokenapi.helper;

public class ResolveReturnFirstTokenData {
  public int Subclass;
  public int Type;
  public int SequenceNo;
  public int BalanceSign;
  public double Balance;
  public int Padding;
}