package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.constants.ClearEventTokenConstants;
import com.aplombtechbd.smartmeter.tokenapi.constants.CommonConstants;
import com.aplombtechbd.smartmeter.tokenapi.utility.*;

public class ClearEventTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;

  public ClearEventTokenGenerationHelper() {
    byteArrayUtility = new ByteArrayUtility();
  }

  public byte[] getPad() {
    byte[] padBytes = new byte[ClearEventTokenConstants.clearEventPadTotalBits];
    for (int i = 0; i < padBytes.length; i++) {
      padBytes[i] = Byte.parseByte(CommonConstants.PadValue);
    }
    return padBytes;
  }

  public byte[] getClearEventTokenDataBlock(byte[] Class, byte[] subClass, byte[] rnd, byte[] seqNo, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : Class) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClass) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : rnd) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNo) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }
}
