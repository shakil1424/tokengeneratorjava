package com.aplombtechbd.smartmeter.tokenapi.helper;

public class ResolveReturnSecondTokenData {
  public int Subclass;
  public int Type;
  public double ForwardActiveEnergy;
  public int ClockSetFlag;
  public int BatteryVoltageLowFlag;
  public int OpenCoverFlag;
  public int OpenBottomCoverFlag;
  public int ByPassFlag;
  public int ReverseFlag;
  public int MagneticInterferenceFlag;
  public int RelayStatusFlag;
  public int RelayFaultFlag;
  public int OverdraftUsedFlag;
}