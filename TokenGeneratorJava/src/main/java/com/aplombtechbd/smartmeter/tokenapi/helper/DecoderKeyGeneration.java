package com.aplombtechbd.smartmeter.tokenapi.helper;

public class DecoderKeyGeneration {
  private TokenGenerationHelper tokenGenerationHelper;

  public DecoderKeyGeneration(){
    tokenGenerationHelper = new TokenGenerationHelper();
  }


  public byte[] getPanBlock(String meterNo) {
    final String IIN = "600727";
    byte[] panBlockBytes = new byte[16];
    if(meterNo.length() == 12)
      meterNo = meterNo.substring(0, meterNo.length() - 1);
    String panString = IIN.substring(1, IIN.length()) + meterNo;

    for (int i = 0; i < 16; ++i) {
      panBlockBytes[15 - i] = Byte.parseByte(panString.substring(i, 1 + i));
    }
    return panBlockBytes;
  }

  public byte[] getControlBlock(String kt, String sgc, String tariffIndex, String krn) {
    byte[] ctrlBlockBytes = new byte[16];

    if (tariffIndex.length() < 2)
      tariffIndex = padTariffIndex(tariffIndex);
    if(sgc.length() < 6)
      sgc = "000000";

    StringBuilder sb = new StringBuilder();
    sb.append(kt);
    sb.append(sgc);
    sb.append(tariffIndex);
    sb.append(krn);

    for (int i = 0; i < 10; i++) {
      ctrlBlockBytes[15 - i] = Byte.parseByte(sb.substring(i, 1 + i));
    }

    for (int i = 0; i < 6; i++) {
      ctrlBlockBytes[5 - i] = 0xF;
    }

    return ctrlBlockBytes;
  }

  private String padTariffIndex(String tariffIndex) {
    return "0" + tariffIndex;
  }

  public byte[] getDecoderKey(String kt, String sgc, String tariffIndex, String krn, String meterNo) {

    final String vendingKeyString = "w9z$C&F)";

    byte[] panBlock = getPanBlock(meterNo);
    byte[] controlBlock = getControlBlock(kt, sgc, tariffIndex, krn);
    byte[] xorOfPanAndCtrl = getXorOfCtrlAndPanBlock(controlBlock, panBlock);
    byte[] eightByteXorOfPanAndCtrl = convertXorOfPanAndCtrlTo8Byte(xorOfPanAndCtrl);

    byte[] vendingKeyBytes = vendingKeyString.getBytes();
    byte[] desEncryptedKey = tokenGenerationHelper.EncryptDataBlock(eightByteXorOfPanAndCtrl, vendingKeyBytes);

    byte[] decoderKey = new byte[8];

    for (int i = 0; i < 8; ++i) {
      decoderKey[i] = (byte) (vendingKeyBytes[i] ^ desEncryptedKey[i]);
    }
    return decoderKey;
  }

  public byte[] convertXorOfPanAndCtrlTo8Byte(byte[] xorOfPanAndCtrl) {
    byte[] eightByteXorOfPanAndCtrl = new byte[8];

    for (int i = 0; i < 8; ++i) {
      eightByteXorOfPanAndCtrl[i] = (byte) (xorOfPanAndCtrl[i * 2] | xorOfPanAndCtrl[i * 2 + 1] << 4);
    }
    return eightByteXorOfPanAndCtrl;
  }

  public byte[] getXorOfCtrlAndPanBlock(byte[] controlBlock, byte[] panBlock) {
    byte[] xorOfPanAndCtrl = new byte[16];

    for (int i = 0; i < 16; i++) {
      xorOfPanAndCtrl[i] = (byte) (controlBlock[i] ^ panBlock[i]);
    }

    return xorOfPanAndCtrl;
  }
}
