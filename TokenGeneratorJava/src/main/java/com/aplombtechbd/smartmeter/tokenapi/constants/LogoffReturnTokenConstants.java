package com.aplombtechbd.smartmeter.tokenapi.constants;

public class LogoffReturnTokenConstants {
  public static final String LogoffReturnClass = "3";
  public static final String LogoffReturnSubClass = "3";
  public static final int LogoffReturnPadTotalBits = 27;
  public static final String LogoffReturnPadValue = "0";
}
