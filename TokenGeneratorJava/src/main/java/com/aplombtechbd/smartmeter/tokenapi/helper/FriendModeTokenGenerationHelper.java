package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.*;

public class FriendModeTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;

  public FriendModeTokenGenerationHelper() {
    byteArrayUtility = new ByteArrayUtility();
  }

  public byte[] getFriendModeDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes, byte[] friendModeBytes,
                                       byte[] hourStart, byte[] hourEnd, byte[] dayBytes, byte[] allowableDaysBytes,
                                       byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : classBytes) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClassBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : friendModeBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : hourStart) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : hourEnd) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : dayBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : allowableDaysBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

  public byte[] getWeekDaysBytes(int[] days) {
    byte[] weekBytes = new byte[]{0x1,0x1,0x1,0x1,0x1,0x1,0x1};
    for(int i = 0; i < days.length; i++){
      weekBytes[days[i]] = 0;
    }
    return weekBytes;
  }
}
