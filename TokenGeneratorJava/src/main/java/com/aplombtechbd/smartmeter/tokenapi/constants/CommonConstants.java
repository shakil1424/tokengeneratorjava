package com.aplombtechbd.smartmeter.tokenapi.constants;

public class CommonConstants {
  public static final String kt = "2"; //key type
  public static final int SeqNoTotalBits = 17;
  public static final  String PadValue = "0";
  public static final int SubClassTotalBits = 4;
  public static final int ClassTotalBits = 2;
  public static final int YearTotalBits = 7;
  public static final int MonthTotalBits = 4;
  public static final int DayTotalBits = 6;
}
