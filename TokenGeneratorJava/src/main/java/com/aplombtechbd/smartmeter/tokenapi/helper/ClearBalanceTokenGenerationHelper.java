package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.*;

public class ClearBalanceTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;

  public ClearBalanceTokenGenerationHelper() {
    byteArrayUtility = new ByteArrayUtility();
  }

  public byte[] getClearBalanceTokenDataBlock(byte[] Class, byte[] subClass, byte[] rnd, byte[] seqNo, byte[] register, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : Class) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClass) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : rnd) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNo) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : register) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }
}
