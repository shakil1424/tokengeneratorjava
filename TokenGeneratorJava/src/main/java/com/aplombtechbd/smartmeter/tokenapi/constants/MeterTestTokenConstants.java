package com.aplombtechbd.smartmeter.tokenapi.constants;

public class MeterTestTokenConstants {
  public static final String MeterTestClass = "1";
  public static final String MeterTestSubClass = "0";
  public static final int PadTotalBits = 31;
  public static final int ManufacturerIdTotalBits = 7;
  public static final int ControlTotalBits = 6;
}
