package com.aplombtechbd.smartmeter.tokenapi.helper;

public class ByteArrayUtility {
  public byte[] formatByteArray(String integerString, int size) {
    return binaryStringToByteArray((getBinaryString(integerString)), size);
  }

  public byte[] formatByteArray(byte[] bytes, int totalBits) {
    String binaryString = getBinaryString(bytes);
    return binaryStringToByteArray(binaryString, totalBits);
  }

  public String getBinaryString(String integerString) {
    return Long.toBinaryString(Long.parseLong(integerString));
  }

  public byte[] binaryStringToByteArray(String binaryString, int totalBit) {

    if (totalBit > binaryString.length()) {
      binaryString = padBinaryString(binaryString, totalBit, "0");
    }
    byte[] bytes = new byte[totalBit];
    int lastChar = binaryString.length() - 1;
    int byteArrayIndex = totalBit - 1;

    for (int i = 0; i < totalBit; i++) {
      bytes[i] = Byte.parseByte(String.valueOf(binaryString.charAt(lastChar - i)));
    }
    return bytes;
  }

  public byte[] binaryStringToEightLengthByteArray(String binaryString, int byteArrayLength) {
    int totalBitsOfByteArray = byteArrayLength * 8;
    if(binaryString.length() < totalBitsOfByteArray)
      binaryString = padBinaryString(binaryString, totalBitsOfByteArray, "0");
    byte[] byteArray = new byte[byteArrayLength];
    for(int i = 0; i < byteArrayLength; i++){
      byteArray[(byteArrayLength-1)-i] = (byte) Integer.parseInt(binaryString.substring(i * 8, (i + 1) * 8), 2);
    }
    return byteArray;
  }

  public String padBinaryString(String originalString, int length, String padCharacter) {
    String paddedString = originalString;
    for (int i = 0; i < length - originalString.length(); i++) {
      paddedString = padCharacter + paddedString;
    }
    return paddedString;
  }


  public long eachByte1BitByteArrayToLong(byte[] bytes) {
    String binaryString = "";
    for (byte b : bytes
            ) {
      binaryString = Integer.toBinaryString(b & 255) + binaryString;
    }
    return getIntegerFromBinaryString(binaryString);
  }

  public long getIntegerFromBinaryString(String binaryString) {
    return Long.parseLong(binaryString, 2);
  }

  public String getBinaryString(byte[] bytes) {
    String binaryString = "";
    String tempString = "";
    for (int i = 0; i < bytes.length; i++) {
      tempString = Integer.toBinaryString((bytes[i] & 255));
      binaryString = tempString + binaryString;
    }
    return binaryString;
  }

  public String get66bitBinaryStringFromTokenByteArray(byte[] bytes) {
    String binaryString = "";
    String tempString = "";
    int i;

    tempString = Integer.toBinaryString((bytes[0] & 255));
    binaryString = binaryString + tempString;

    for (i = 1; i < bytes.length; i++) {
      tempString = Integer.toBinaryString((bytes[i] & 255));
      binaryString = binaryString + padBinaryString(tempString, 8, "0");
    }

    if(binaryString.length() < 66){
      return padBinaryString(binaryString, 66, "0");
    }
    else
      return binaryString;
  }

  public String getPaddedBinaryString(byte[] bytes) {
    String binaryString = "";
    String tempString = "";
    for (int i = 0; i < bytes.length; i++) {
      tempString = Integer.toBinaryString((bytes[i] & 255));
      binaryString = padBinaryString(tempString, 8, "0") + binaryString;
    }
    return binaryString;
  }

  public byte[] convert64bitDataBlockTo8ByteDataBlock(byte[] dataBlock) {
    String binaryString64bit = getBinaryString(dataBlock);
    byte[] eightByteDataBlock = new byte[8];
    for (int i = 0; i < 8; i++) {
      eightByteDataBlock[7-i] = (byte) Integer.parseInt(binaryString64bit.substring(i * 8, (1 + i) * 8), 2);
    }
    return eightByteDataBlock;
  }

  public byte[] convert8ByteDataBlockTo64bitDataBlock(byte[] dataBlock8Byte){
    byte[] dataBlock = new byte[64];
    String binaryString64Bit = getPaddedBinaryString(dataBlock8Byte);
    for(int i = 0; i < 64; i++){
      dataBlock[(binaryString64Bit.length()-1)-i] = (byte) Integer.parseInt(binaryString64Bit.substring(i,i+1),2);
    }
    return dataBlock;
  }

  public byte[] mapBinaryStringToByteArray(byte[] byteArray, String binaryString) {
    for (int i = 0; i < binaryString.length(); i++) {
      byteArray[i] = (byte) Integer.parseInt(binaryString.substring(i, (i + 1)), 2);
    }
    return byteArray;
  }

  public byte[] convert50BitDataBlockTo7Byte(byte[] dataBlock50Bit) {
    String dataBlockString = "";
    for (byte aDataBlock50Bit : dataBlock50Bit) {

      dataBlockString = String.valueOf(aDataBlock50Bit)+dataBlockString;
    }
    dataBlockString = "000000"+dataBlockString;
    byte[] sevenByteDataBlock = new byte[7];

    for (int i = 0; i < 7; i++) {
      sevenByteDataBlock[6-i] = (byte) Integer.parseInt(dataBlockString.substring(i * 8, (i + 1) * 8), 2);
    }
    return sevenByteDataBlock;
  }

  public static void main(String[] args) {
    ByteArrayUtility bArrGen = new ByteArrayUtility();
    byte[] b = new byte[]{127, 9};
    System.out.println(bArrGen.getBinaryString(b));
  }
}
