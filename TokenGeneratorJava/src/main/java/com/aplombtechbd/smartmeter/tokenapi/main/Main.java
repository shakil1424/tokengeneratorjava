package com.aplombtechbd.smartmeter.tokenapi.main;

import com.aplombtechbd.smartmeter.tokenapi.AplTokenApi;
import com.aplombtechbd.smartmeter.tokenapi.TokenImplementation;

public class Main {

  public static void main(String[] args) {
    AplTokenApi tokenApi = new TokenImplementation();
    System.out.println(
            tokenApi.getStepTariffToken("541610025320", "999910", 1, 1, 255, 1,
                    1, "2018-04-25", 1, 0, new int[]{380, 514, 536, 563, 870, 998},
                    new int[]{75, 200, 300, 400, 600}, new int[]{0, 0, 0, 0, 0, 0, 0, 0}));
    System.out.println(
            tokenApi.getCreditToken("541610025320", "999910", 1, 1, 1, 1, 1,
                    10000));
    System.out.println(
            tokenApi.getFriendModeToken("541610025320", "999910", 1, 1, 1, 1,
                    1, 1, new int[]{10, 20}, new int[]{6, 0, 3}, 1));
    System.out.println(
            tokenApi.getKeyChangeToken(
                    "541610025320", "999910", 1, 1, 1, 1, "999999", 1, 1, 255, 1));
    System.out.println(
            tokenApi.getSetCreditAmountLimitOrOverdrawAmountLimitToken("541610025320", "999910", 1,
                    1, 1, 1, 1, 0, 10000));
    System.out.println(
            tokenApi.getSetLowCreditWarningLimitToken("541610025320", "999910", 1, 1, 1,
                    1, 1, 2, 20000));
    System.out.println(
            tokenApi.getSetCreditAmountLimitOrOverdrawAmountLimitToken("541610025320", "999910", 1
                    , 1, 1, 1, 1, 1, 500000));
    System.out.println(
            tokenApi.getClearEventToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1));
    System.out.println(
            tokenApi.getClearBalanceToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1));
    System.out.println(
            tokenApi.getMaxPowerLimitToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1, 1,
                    "2018-10-11", new int[]{100, 200}, new int[]{17, 23}));
    System.out.println(
            tokenApi.getSingleTariffToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1, "2018-01-01",
                    1, 0, 550));
    System.out.println(
            tokenApi.getTOUTariffToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1, "2018-01-01",
                    1, 0, new int[]{1, 2, 3}, new int[]{5, 10, 23}));
    System.out.println(
            tokenApi.generateSwitchModeN2PToken(
                    "541610025320", "999910", 1, 1, 1, 1, 1, 0));

    System.out.println(
            tokenApi.getLogoffReturnToken(
                    "541610025320", "999910", 1, 1, 1, 1, 100));

    System.out.println(
            tokenApi.getHolidayModeToken(
                    "541610025320", "999910", 1, 1, 1, 1, 100, 1,
                    new String[]{"2017-03-17", "2018-12-25", "2018-04-14"}));

    System.out.println(tokenApi.getTestToken(99, 14));

//    TokenImplementation tokenGenerator = new TokenImplementation();
//    ResolveReturnTokenGenerationHelper resolve = new ResolveReturnTokenGenerationHelper();
//    TokenGenerationHelper tgHelper = new TokenGenerationHelper();
//    byte[] datablock = new byte[]
//            {
//                    0, 0, 1, 0,
//                    1,
//                    0, 0, 1, 0, 0, 1, 1, 0,
//                    1,
//                    0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0
//            };
//    byte[] datablock2 = new byte[]
//            {
//                    0, 0, 1, 0,
//                    0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
//                    0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0
//            };
//    ResolveReturnFirstTokenData data = resolve.getFirstTokenData(datablock);
//    ResolveReturnSecondTokenData data2 = resolve.getSecondTokenData(datablock2);
//
//
//    System.out.println();
//    System.out.println(tgHelper.generateResolveReturnTokenXml(data,data2,99));
//    System.out.println();
  }
}
