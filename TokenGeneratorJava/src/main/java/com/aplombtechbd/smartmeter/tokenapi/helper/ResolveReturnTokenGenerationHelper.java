package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.DES;

import java.math.BigInteger;

public class ResolveReturnTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;
  private TokenGenerationHelper tokenGenerationHelper;
  private DES des;

  public ResolveReturnTokenGenerationHelper() {
    byteArrayUtility = new ByteArrayUtility();
    tokenGenerationHelper = new TokenGenerationHelper();
    des = new DES();
  }

  public ResolveReturnFirstTokenData getFirstTokenData(byte[] dataBlock) {
    byte[] subclass = new byte[4];
    byte[] type = new byte[1];
    byte[] sequenceNo = new byte[8];
    byte[] balanceSign = new byte[1];
    byte[] balance = new byte[27];
    byte[] padding = new byte[7];
    int index = -1;

    for (int i = 0; i < subclass.length; i++) {
      subclass[i] = dataBlock[++index];
    }

    for (int i = 0; i < type.length; i++) {
      type[i] = dataBlock[++index];
    }

    for (int i = 0; i < sequenceNo.length; i++) {
      sequenceNo[i] = dataBlock[++index];
    }

    for (int i = 0; i < balanceSign.length; i++) {
      balanceSign[i] = dataBlock[++index];
    }

    for (int i = 0; i < balance.length; i++) {
      balance[i] = dataBlock[++index];
    }

    for (int i = 0; i < padding.length; i++) {
      padding[i] = dataBlock[++index];
    }

    ResolveReturnFirstTokenData firstTokenData = new ResolveReturnFirstTokenData();

    firstTokenData.Subclass = (int) byteArrayUtility.eachByte1BitByteArrayToLong(subclass);
    firstTokenData.Type = (int) byteArrayUtility.eachByte1BitByteArrayToLong(type);
    firstTokenData.SequenceNo = (int) byteArrayUtility.eachByte1BitByteArrayToLong(sequenceNo);
    firstTokenData.BalanceSign = (int) byteArrayUtility.eachByte1BitByteArrayToLong(balanceSign);
    firstTokenData.Balance = byteArrayUtility.eachByte1BitByteArrayToLong(balance) / 100.0;
    firstTokenData.Padding = (int) byteArrayUtility.eachByte1BitByteArrayToLong(padding);

    return firstTokenData;
  }

  public ResolveReturnSecondTokenData getSecondTokenData(byte[] dataBlock) {
    int index = -1;
    byte[] subclass = new byte[4];
    byte[] type = new byte[1];
    byte[] forwardActiveEnergy = new byte[32];
    byte[] clockSetFlag = new byte[1];
    byte[] batteryVoltageLowFlag = new byte[1];
    byte[] openCoverFlag = new byte[1];
    byte[] openBottomCoverFlag = new byte[1];
    byte[] byPassFlag = new byte[1];
    byte[] reverseFlag = new byte[1];
    byte[] magneticInterferenceFlag = new byte[1];
    byte[] relayStatusFlag = new byte[1];
    byte[] relayFaultFlag = new byte[1];
    byte[] overdraftUsedFlag = new byte[1];
    byte[] padding = new byte[1];

    for (int i = 0; i < subclass.length; i++) {
      subclass[i] = dataBlock[++index];
    }

    for (int i = 0; i < type.length; i++)
    {
      type[i] = dataBlock[++index];
    }

    for (int i = 0; i < forwardActiveEnergy.length; i++) {
      forwardActiveEnergy[i] = dataBlock[++index];
    }

    for (int i = 0; i < clockSetFlag.length; i++) {
      clockSetFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < batteryVoltageLowFlag.length; i++) {
      batteryVoltageLowFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < openCoverFlag.length; i++) {
      openCoverFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < openBottomCoverFlag.length; i++) {
      openBottomCoverFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < byPassFlag.length; i++) {
      byPassFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < reverseFlag.length; i++) {
      reverseFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < magneticInterferenceFlag.length; i++) {
      magneticInterferenceFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < relayStatusFlag.length; i++) {
      relayStatusFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < relayFaultFlag.length; i++) {
      relayFaultFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < overdraftUsedFlag.length; i++) {
      overdraftUsedFlag[i] = dataBlock[++index];
    }

    for (int i = 0; i < padding.length; i++) {
      padding[i] = dataBlock[++index];
    }

    ResolveReturnSecondTokenData secondTokenData = new ResolveReturnSecondTokenData();

    secondTokenData.Subclass = (int) byteArrayUtility.eachByte1BitByteArrayToLong(subclass);
    secondTokenData.Type = (int)byteArrayUtility.eachByte1BitByteArrayToLong(type);
    secondTokenData.ForwardActiveEnergy = byteArrayUtility.eachByte1BitByteArrayToLong(forwardActiveEnergy) / 100.0;
    secondTokenData.ClockSetFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(clockSetFlag);
    secondTokenData.BatteryVoltageLowFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(batteryVoltageLowFlag);
    secondTokenData.OpenCoverFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(openCoverFlag);
    secondTokenData.OpenBottomCoverFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(openBottomCoverFlag);
    secondTokenData.ByPassFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(byPassFlag);
    secondTokenData.ReverseFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(reverseFlag);
    secondTokenData.MagneticInterferenceFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(magneticInterferenceFlag);
    secondTokenData.RelayStatusFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(relayStatusFlag);
    secondTokenData.RelayFaultFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(relayFaultFlag);
    secondTokenData.OverdraftUsedFlag = (int) byteArrayUtility.eachByte1BitByteArrayToLong(overdraftUsedFlag);

    return secondTokenData;
  }

  public byte[] getReturnTokenDecryptedDataBlock(String token20Digit, byte[] decoderKey) {
    BigInteger tokenBigInteger = new BigInteger(token20Digit);
    byte[] tokenBytes = tokenBigInteger.toByteArray();
    String tokenBinaryString66Bits = byteArrayUtility.get66bitBinaryStringFromTokenByteArray(tokenBytes);
    String reversedTokenBinaryString = tokenGenerationHelper.forMatBinaryString(tokenBinaryString66Bits);
    byte[] token66bits = new byte[66];
    byteArrayUtility.mapBinaryStringToByteArray(token66bits, reversedTokenBinaryString);

    token66bits[28] = token66bits[65];
    token66bits[27] = token66bits[64];

    String dataBlockBinaryString = byteArrayUtility.getBinaryString(token66bits);

    byte[] encryptedByteArray = byteArrayUtility.binaryStringToEightLengthByteArray(dataBlockBinaryString.substring(2, 66), 8);
    byte[] decryptedByteArray = DES.ECB_Decrypt(encryptedByteArray, decoderKey);

    return byteArrayUtility.convert8ByteDataBlockTo64bitDataBlock(decryptedByteArray);
  }
}
