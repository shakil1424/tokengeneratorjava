package com.aplombtechbd.smartmeter.tokenapi.constants;

public class CreditAmoutLimitConstants {
  public static final int creditAmountLimitAmountTotalBits = 27;
  public static final String creditAmountLimitClass = "2";
  public static final String creditAmountLimitSubClass = "13";
}
