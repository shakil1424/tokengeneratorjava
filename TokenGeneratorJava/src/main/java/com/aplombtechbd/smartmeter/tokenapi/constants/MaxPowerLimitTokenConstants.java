package com.aplombtechbd.smartmeter.tokenapi.constants;

public class MaxPowerLimitTokenConstants {
  public static final String maxPowerLimitClass = "2";
  public static final String maxPowerLimitSubClass = "2";
  public static final int maxPowerLimitHourTotalBits = 5;
  public static final int maxPowerLimitMPLtotalBits = 16;
  public static final int maxPowerLimitPadTotalBits = 6;

  public static final String maxPowerLimitActiveModeClass = "2";
  public static final String maxPowerLimitActiveModeSubClass = "3";
  public static final int MaxPowerLimitsLengthTotalBits = 9;
  public static final int maxPowerLimitActiveModelTotalBits = 1;
}
