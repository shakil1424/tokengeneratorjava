package com.aplombtechbd.smartmeter.tokenapi.constants;

public class FriendModeTokenConstants {
  public static final String FriendModeClass = "2";
  public static final String FriendModeSubClass = "11";
  public static final int FriendModePadlBits = 2;
  public static final int FriendModeDaysTotalBits = 7;
  public static final int FriendModeAllowableDaysTotalBits = 7;
  public static final int FriendModeHourStartTotalBits = 5;
  public static final int FriendModeHourEndTotalBits = 5;
  public static final int FriendModeTotalBits = 1;
  public static final String FriendModeActiveClass = "3";
  public static final String FriendModeActiveSubClass = "4";
}
