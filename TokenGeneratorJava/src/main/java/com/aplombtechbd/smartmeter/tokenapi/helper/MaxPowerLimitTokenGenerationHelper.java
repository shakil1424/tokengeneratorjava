package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.*;

public class MaxPowerLimitTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;
  public MaxPowerLimitTokenGenerationHelper(){
    byteArrayUtility = new ByteArrayUtility();
  }

  public byte[] getMaxPowerLimitDataBlock(byte[] Class, byte[] subClass, byte[] seqNoBytes, byte[] hour, byte[] mpl, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : Class) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClass) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : hour) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : mpl) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

  public byte[] getMaxPowerLimitActiveModeDataBlock(byte[] Class, byte[] subClass, byte[] seqNoBytes, byte[] activeModelbytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes, byte[] maxPowerLimitsLength) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : Class) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClass) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : activeModelbytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : yearBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : monthBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : dayBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : maxPowerLimitsLength) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

}
