package com.aplombtechbd.smartmeter.tokenapi.constants;

public class KeyChangeTokenConstants {
  public static final String token1Class = "2";
  public static final String token1SubClass = "0";
  public static final String KENHO_value = "15";
  public static final int KENHO_totalBits = 4;
  public static final String KRN_value = "1";
  public static final int KRN_totalBits = 4;
  public static final int RO_totalBits = 1;
  public static final String RO_value_0 = "0";
  public static final String RO_value_1 = "1";
  public static final String Res_value = "0";
  public static final int Res_totalBits = 1;
  public static final String KT_value = "2";
  public static final int KT_totalBits = 2;
  public static final int NKHO_totalBits = 32;

  public static final String token2Class = "2";
  public static final String token2SubClass = "1";
  public static final String KENLO_value = "15";
  public static final int KENLO_totalBits = 4;
  public static final int TI_totalBits = 8;
  public static final int NKLO_totalBits = 32;
}
