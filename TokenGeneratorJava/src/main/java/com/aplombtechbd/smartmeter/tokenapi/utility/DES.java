package com.aplombtechbd.smartmeter.tokenapi.utility;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class DES {
    public DES() {
    }

    public static byte[] generateKey() {
        try {
            SecureRandom e = new SecureRandom();
            KeyGenerator kg = KeyGenerator.getInstance("DES");
            kg.init(e);
            SecretKey secretKey = kg.generateKey();
            byte[] key = secretKey.getEncoded();
            return key;
        } catch (NoSuchAlgorithmException var4) {
            System.err.println("DES generateKey!");
            var4.printStackTrace();
            return null;
        }
    }

    public static byte[] ECB_EncryptTo(byte[] data, byte[] key) {
        try {
            SecureRandom e = new SecureRandom();
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            cipher.init(1, secretKey, e);
            byte[] encryptedData = cipher.doFinal(data);
            return encryptedData;
        } catch (Exception var8) {
            System.err.println("DES ECB_Encrypt!");
            var8.printStackTrace();
            return null;
        }
    }

    public static byte[] ECB_Encrypt(String data, byte[] key) {
        try {
            SecureRandom e = new SecureRandom();
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(1, secretKey, e);
            byte[] encryptedData = cipher.doFinal(data.getBytes());
            return encryptedData;
        } catch (Exception var8) {
            System.err.println("DES ECB_Encrypt!");
            var8.printStackTrace();
            return null;
        }
    }

    public static byte[] ECB_Decrypt(byte[] data, byte[] key) {
        try {
            SecureRandom e = new SecureRandom();
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            cipher.init(2, secretKey, e);
            byte[] decryptedData = cipher.doFinal(data);
            return decryptedData;
        } catch (Exception var8) {
            System.err.println("DES ECB_Decrypt");
            var8.printStackTrace();
            return null;
        }
    }

    public static String ECB_DecryptToStr(byte[] data, byte[] key) {
        try {
            SecureRandom e = new SecureRandom();
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(2, secretKey, e);
            byte[] decryptedData = cipher.doFinal(data);
            return new String(decryptedData);
        } catch (Exception var8) {
            System.err.println("DES ECB_DecryptToStr");
            var8.printStackTrace();
            return null;
        }
    }

    public static byte[] CBC_Encrypt(byte[] data, byte[] key, byte[] iv) {
        try {
            DESKeySpec e = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(e);
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            IvParameterSpec param = new IvParameterSpec(iv);
            cipher.init(1, secretKey, param);
            byte[] encryptedData = cipher.doFinal(data);
            return encryptedData;
        } catch (Exception var9) {
            System.err.println("DES CBC_Encrypt!");
            var9.printStackTrace();
            return null;
        }
    }

    public static byte[] CBC_Decrypt(byte[] data, byte[] key, byte[] iv) {
        try {
            DESKeySpec e = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(e);
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            IvParameterSpec param = new IvParameterSpec(iv);
            cipher.init(2, secretKey, param);
            byte[] decryptedData = cipher.doFinal(data);
            return decryptedData;
        } catch (Exception var9) {
            System.err.println("DES CBC_Decrypt");
            var9.printStackTrace();
            return null;
        }
    }

    public static byte[] partyKey(byte[] key, String pt) {
        if (!pt.equalsIgnoreCase("even") && !pt.equalsIgnoreCase("odd")) {
            return key;
        } else {
            byte[] ck = new byte[key.length];

            for(int i = 0; i < key.length; ++i) {
                int count = 0;
                ck[i] = (byte)(key[i] & 254);

                for(int p = 1; p < 8; ++p) {
                    if ((ck[i] >>> p & 1) == 1) {
                        ++count;
                    }
                }

                if (pt.equalsIgnoreCase("even") && count % 2 != 0) {
                    ck[i] = (byte)(ck[i] | 1);
                }

                if (pt.equalsIgnoreCase("odd") && count % 2 == 0) {
                    ck[i] = (byte)(ck[i] | 1);
                }
            }

            return ck;
        }
    }

    public static void formatIntTo16(byte[] bytes) {
        String[] resultBytes = new String[bytes.length];

        for(int i = 0; i < bytes.length; ++i) {
            resultBytes[i] = Integer.toHexString(bytes[i] & 255);
        }

    }

    public static void main(String[] args) {
        try {
            byte[] e = "11111111".getBytes();
            byte[] iv = "22222222".getBytes();
            byte[] data = ECB_EncryptTo("ebc mode test".getBytes(), e);
            System.out.print("EBC mode:");
            System.out.println(new String(ECB_Decrypt(data, e)));
            System.out.print("CBC mode:");
            data = CBC_Encrypt("cbc mode test".getBytes(), e, iv);
            System.out.println(new String(CBC_Decrypt(data, e, iv)));
            System.out.println(generateKey());
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }
}
