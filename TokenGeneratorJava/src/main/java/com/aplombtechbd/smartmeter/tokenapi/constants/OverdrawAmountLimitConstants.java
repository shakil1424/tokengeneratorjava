package com.aplombtechbd.smartmeter.tokenapi.constants;

public class OverdrawAmountLimitConstants {
  public static final int overdrawAmountLimitAmountTotalBits = 27;
  public static final String overdrawAmountClass = "2";
  public static final String overdrawAmountSubClass = "14";
}
