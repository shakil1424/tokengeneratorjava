package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.CRC;

public class MeterTestTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;

  public MeterTestTokenGenerationHelper(){
    byteArrayUtility = new ByteArrayUtility();
  }
  public byte[] getMeterTestTokenDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] manufacturerIdBytes, byte[] controlBytes, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : classBytes) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClassBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : manufacturerIdBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : controlBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

}
