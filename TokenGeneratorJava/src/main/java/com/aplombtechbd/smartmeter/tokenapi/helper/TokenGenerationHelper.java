package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.DES;

import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.Random;

public class TokenGenerationHelper {

  public boolean hasDecimalPoint(double amount) {
    return (amount % 1) != 0;
  }

  public int getFourBitRandomNumber() {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.set(1993, 1, 1, 0, 0, 0);
    long tick = calendar.getTimeInMillis();
    Random rand = new Random(tick & 4294967295L);
    int randomNum = rand.nextInt();
    return ((randomNum & 255));
  }

  public byte[] formatByteArray(byte[] b, int totalBit) {

    int len = b.length;
    byte[] tempByte = new byte[totalBit];
    int k;
    if (len < totalBit) {
      for (k = 0; k < totalBit; ++k) {
        tempByte[k] = b[k];
        if (k + 1 == len) {
          tempByte[k] = 0;
        }
      }
    } else if (len == totalBit || len > totalBit) {
      for (k = 0; k < totalBit; ++k) {
        tempByte[k] = b[k];
      }
    }

    for (k = 0; k < tempByte.length / 2; ++k) {
      byte temp = tempByte[k];
      tempByte[k] = tempByte[tempByte.length - 1 - k];
      tempByte[tempByte.length - 1 - k] = temp;
    }

    return tempByte;
  }

  public BigInteger token66BitsToDecimal(byte[] token66bits) {
    StringBuffer sb = new StringBuffer();

    for (byte b : token66bits) {
      sb.append(String.valueOf(b));
    }
    return new BigInteger(sb.toString(), 2);
  }

  public String generateTokenXml(int errorCode, String[] tokens) {
    StringBuffer sbuf = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?><result>");
    sbuf.append("<errorCode>");
    sbuf.append(errorCode);
    sbuf.append("</errorCode>");
    sbuf.append("<tokens>");
    if (tokens == null) {
      sbuf.append("<token></token>");
    } else {
      for (String token : tokens) {
        if ((token != null) && !token.equals("")) {
          sbuf.append("<token>").append(token).append("</token>");
        }
      }
    }
    sbuf.append("</tokens>");
    sbuf.append("</result>");
    return sbuf.toString();
  }

  public String transposeClassBits(String encryptedBinaryString, byte[] classBytes) {
    StringBuilder originalTokenString = new StringBuilder(forMatBinaryString(encryptedBinaryString));
    StringBuilder tokenBinaryString66Bits = new StringBuilder();
    tokenBinaryString66Bits.append(originalTokenString.charAt(27));
    tokenBinaryString66Bits.append(originalTokenString.charAt(28));

    originalTokenString.setCharAt(28, String.valueOf(classBytes[1]).charAt(0));
    originalTokenString.setCharAt(27, String.valueOf(classBytes[0]).charAt(0));

    tokenBinaryString66Bits.insert(0,originalTokenString);
    return forMatBinaryString(tokenBinaryString66Bits.toString());
  }

  public String padTokenBigInteger(int length, String token) {
    int size = length - token.length();
    if (size > 0) {
      for (int i = 0; i < size; ++i) {
        token = "0" + token;
      }
    }
    return token;
  }

  public String getTokenBigInteger(String token66bitsBinaryString) {
    BigInteger tokenBigInteger = new BigInteger((token66bitsBinaryString), 2);
    return padTokenBigInteger(20, tokenBigInteger.toString());
  }

  public long getSeqNo(long keyNo, int seqNo) {
    long calculatedSeqNo;
    long seqNoHeight9Bits_keyNo = keyNo & 511L;
    int seqNoLow8Bits_seqNo = seqNo & 255;
    calculatedSeqNo = (seqNoHeight9Bits_keyNo << 8) + (long) seqNoLow8Bits_seqNo;
    return calculatedSeqNo;
  }

  public byte[] EncryptDataBlock(byte[] data, byte[] key) {
    return DES.ECB_EncryptTo(data, key);
  }

  public String forMatBinaryString(String binaryString){
    String reverseStringAsBinaryForm = "";
    for (int i = 0; i < binaryString.length(); i++)
      reverseStringAsBinaryForm += binaryString.substring(binaryString.length() - (i + 1), binaryString.length() - i);

    return reverseStringAsBinaryForm;
  }

  public String generateResolveReturnTokenXml(ResolveReturnFirstTokenData firstTokenData,
                                              ResolveReturnSecondTokenData secondTokenData, int tariffIndex) {
    StringBuilder stringBuilder = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

    stringBuilder.append("<result>");

    stringBuilder.append("<type>");
    stringBuilder.append(firstTokenData.Type);
    stringBuilder.append("</type>");

    stringBuilder.append("<balance>");
    if (firstTokenData.BalanceSign == 1)
      stringBuilder.append("-");
    stringBuilder.append(String.format ("%.2f", firstTokenData.Balance));
    stringBuilder.append("</balance>");

    stringBuilder.append("<sequence>");
    stringBuilder.append(firstTokenData.SequenceNo);
    stringBuilder.append("</sequence>");

    stringBuilder.append("<event>");

    stringBuilder.append("<clockSetFlag>");
    stringBuilder.append(secondTokenData.ClockSetFlag);
    stringBuilder.append("</clockSetFlag>");

    stringBuilder.append("<batteryVoltageLowFlag>");
    stringBuilder.append(secondTokenData.BatteryVoltageLowFlag);
    stringBuilder.append("</batteryVoltageLowFlag>");

    stringBuilder.append("<openCoverFlag>");
    stringBuilder.append(secondTokenData.OpenCoverFlag);
    stringBuilder.append("</openCoverFlag>");

    stringBuilder.append("<openBottomCoverFlag>");
    stringBuilder.append(secondTokenData.OpenBottomCoverFlag);
    stringBuilder.append("</openBottomCoverFlag>");

    stringBuilder.append("<byPassFlag>");
    stringBuilder.append(secondTokenData.ByPassFlag);
    stringBuilder.append("</byPassFlag>");

    stringBuilder.append("<reverseFlag>");
    stringBuilder.append(secondTokenData.ReverseFlag);
    stringBuilder.append("</reverseFlag>");

    stringBuilder.append("<magneticInterfereFlag>");
    stringBuilder.append(secondTokenData.MagneticInterferenceFlag);
    stringBuilder.append("</magneticInterfereFlag>");

    stringBuilder.append("<relayStatusFlag>");
    stringBuilder.append(secondTokenData.RelayStatusFlag);
    stringBuilder.append("</relayStatusFlag>");

    stringBuilder.append("<relayFaultFlag>");
    stringBuilder.append(secondTokenData.RelayFaultFlag);
    stringBuilder.append("</relayFaultFlag>");

    stringBuilder.append("<overdraftUsedFlag>");
    stringBuilder.append(secondTokenData.OverdraftUsedFlag);
    stringBuilder.append("</overdraftUsedFlag>");

    stringBuilder.append("<forwardActiveEnergyTol>");
    stringBuilder.append(secondTokenData.ForwardActiveEnergy);
    stringBuilder.append("</forwardActiveEnergyTol>");

    stringBuilder.append("<tariffIndex>");
    stringBuilder.append(tariffIndex);
    stringBuilder.append("</tariffIndex>");

    stringBuilder.append("</event>");
    stringBuilder.append("</result>");

    return stringBuilder.toString();
  }
}
