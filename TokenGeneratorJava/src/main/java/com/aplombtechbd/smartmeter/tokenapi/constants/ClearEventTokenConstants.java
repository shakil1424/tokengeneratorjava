package com.aplombtechbd.smartmeter.tokenapi.constants;

public class ClearEventTokenConstants {
  public static final int clearEventRndTotalBits = 4;
  public static final int clearEventPadTotalBits = 23;
  public static final String clearEventClass = "3";
  public static final String clearEventSubClass = "0";
}
