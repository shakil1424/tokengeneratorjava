package com.aplombtechbd.smartmeter.tokenapi.constants;

public class ClearBalanceTokenConstants {
  public static final int clearBalanceRndTotalBits = 4;
  public static final int clearBalancePadTotalBits = 7;
  public static final int clearBalanceRegisterTotalBits = 16;
  public static final String clearBalanceRegisterValue = "0";
  public static final String clearBalanceClass = "3";
  public static final String clearBalanceSubClass = "1";
}
