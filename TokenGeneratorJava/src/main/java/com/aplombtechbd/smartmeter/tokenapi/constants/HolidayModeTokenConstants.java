package com.aplombtechbd.smartmeter.tokenapi.constants;

public class HolidayModeTokenConstants {
  public static String holidayModeClass = "2";
  public static String holidayModeSubClass = "12";
  public static int holidayModePadTotalBits = 5;
  public static int holidayModeRndTotalBits = 4;
  public static int holidayModeTotalBits = 1;
}
