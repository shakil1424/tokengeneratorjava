package com.aplombtechbd.smartmeter.tokenapi.helper;

import com.aplombtechbd.smartmeter.tokenapi.utility.CRC;

public class StepTariffTokenGenerationHelper {
  private ByteArrayUtility byteArrayUtility;

  public StepTariffTokenGenerationHelper() {
    byteArrayUtility = new ByteArrayUtility();
  }

  public byte[] getStepTariffRateSettingsDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes, byte[] step, byte[] rate, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : classBytes) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClassBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : step) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : rate) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

  public byte[] getStepTariffActiveModeDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes, byte[] activeModelBytes, byte[] validateBytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes, byte[] ratesLength) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : classBytes) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClassBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : activeModelBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : validateBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : yearBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : monthBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : dayBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : ratesLength) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

  public byte[] getStepTariffFlagSettingsDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes, byte[] flag, byte[] pad) {
    byte[] dataBlock64Bit = new byte[64];
    byte[] dataBlock50Bit = new byte[50];

    int bitPosition50BitBlock = -1;
    int bitPosition64BitBlock = -1;

    for (byte bit : classBytes) {
      ++bitPosition50BitBlock;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : subClassBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : seqNoBytes) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : flag) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    for (byte bit : pad) {
      ++bitPosition50BitBlock;
      ++bitPosition64BitBlock;

      dataBlock64Bit[bitPosition64BitBlock] = bit;
      dataBlock50Bit[bitPosition50BitBlock] = bit;
    }

    byte[] dataBlock7Byte = byteArrayUtility.convert50BitDataBlockTo7Byte(dataBlock50Bit);

    CRC cCRC = new CRC();
    cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

    return dataBlock64Bit;
  }

  public byte[] getFlagBytes(int[] flag) {
    byte[] flagBytes = new byte[8];
    for (int i = 0; i < 8; i++){
      flagBytes[i] = (byte)flag[i];
    }
    return flagBytes;
  }
}
