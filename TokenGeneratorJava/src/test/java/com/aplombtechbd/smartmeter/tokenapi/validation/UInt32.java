package com.aplombtechbd.smartmeter.tokenapi.validation;


import java.io.Serializable;
import java.math.BigInteger;

public class UInt32 implements Serializable {
  private static final long serialVersionUID = -4984964803548518342L;
  private BigInteger value;
  private byte[] by = new byte[32];
  private String byStr;

  //added method
  public static byte[] getByte(String value){
    return new UInt32(value).getBy();
  }

  public BigInteger getValue() {
    return this.value;
  }

  public byte[] getBy() {
    return this.by;
  }

  public String getByStr() {
    return this.byStr;
  }

  public UInt32(String value) {
    this.value = new BigInteger(value);
    this.byStr = this.value.toString(2);
    this.byStr = this.addStr(this.byStr);
    this.by = stringToByte(this.byStr);
  }

  public UInt32(BigInteger bi) {
    this.value = bi;
    this.byStr = this.value.toString(2);
    this.byStr = this.addStr(this.byStr);
    this.by = stringToByte(this.byStr);
  }

  public static UInt32 getNot(UInt32 uint) {
    byte[] bynew = new byte[32];

    for(int bi = 0; bi < uint.by.length; ++bi) {
      bynew[bi] = (byte)(1 - uint.by[bi]);
    }

    BigInteger var3 = towToTen(bynew);
    return new UInt32(var3);
  }

  public static UInt32 getShiftLeft(UInt32 uint, int n) {
    String newStr = uint.getByStr();

    for(int by = 0; by < n; ++by) {
      newStr = newStr + "0";
    }

    byte[] var4 = stringToByte(newStr);
    return new UInt32(towToTen(var4));
  }

  public static UInt32 getShiftRight(UInt32 uint, int n) {
    return new UInt32(uint.value.shiftRight(n));
  }

  public String toString() {
    return this.value.toString();
  }

  public static UInt32 getOr(UInt32 uint1, UInt32 uint2) {
    byte[] by = new byte[32];

    for(int i = 0; i < by.length; ++i) {
      by[i] = (byte)(uint1.getBy()[i] | uint2.getBy()[i]);
    }

    return new UInt32(towToTen(by));
  }

  public static UInt32 getAnd(UInt32 uint1, UInt32 uint2) {
    byte[] by = new byte[32];

    for(int i = 0; i < by.length; ++i) {
      by[i] = (byte)(uint1.getBy()[i] & uint2.getBy()[i]);
    }

    return new UInt32(towToTen(by));
  }

  private static BigInteger towToTen(byte[] by) {
    BigInteger bi = new BigInteger("0");

    for(int i = 0; i < by.length; ++i) {
      bi = bi.multiply(new BigInteger("2")).add(new BigInteger(String.valueOf(by[31 - i])));
    }

    return bi;
  }

  private static byte[] stringToByte(String str) {
    str = str.substring(str.length() - 32);
    byte[] by = new byte[32];

    for(int i = 0; i < 32; ++i) {
      by[i] = (byte)Integer.parseInt(String.valueOf(str.charAt(31 - i)));
    }

    return by;
  }

  private String addStr(String str) {
    String newStr = str;

    for(int i = str.length(); i < 32; ++i) {
      newStr = "0" + newStr;
    }

    return newStr;
  }

  public static byte[] getOriginalBy(UInt32 uint) {
    String str = uint.getValue().toString(2);
    byte[] by = new byte[str.length()];

    for(int i = 0; i < str.length(); ++i) {
      by[i] = (byte)Integer.parseInt(String.valueOf(str.charAt(str.length() - 1 - i)));
    }

    return by;
  }

  public static UInt32 convert16ToUInt32(String str) {
    BigInteger bi = new BigInteger(str, 16);
    return new UInt32(bi);
  }

  public UInt32 convert2ToUInt32(String str) {
    BigInteger bi = new BigInteger(str, 2);
    return new UInt32(bi);
  }

  public UInt32 convert8ToUInt32(String str) {
    BigInteger bi = new BigInteger(str, 8);
    return new UInt32(bi);
  }
}
