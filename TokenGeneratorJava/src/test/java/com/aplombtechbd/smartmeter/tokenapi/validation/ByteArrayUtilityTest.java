package com.aplombtechbd.smartmeter.tokenapi.validation;

import org.junit.Assert;
import org.junit.Test;
import com.aplombtechbd.smartmeter.tokenapi.helper.ByteArrayUtility;
import com.aplombtechbd.smartmeter.tokenapi.helper.TokenGenerationHelper;

import java.util.Arrays;

public class ByteArrayUtilityTest {
  private ByteArrayUtility byteArrayUtility;
  private TokenGenerationHelper tokenGenerationHelper;

  public ByteArrayUtilityTest() {
    byteArrayUtility = new ByteArrayUtility();
    tokenGenerationHelper = new TokenGenerationHelper();
  }

  @Test
  public void getBinaryStringTest() {
    Assert.assertEquals("11111111", byteArrayUtility.getBinaryString("255"));
    Assert.assertEquals("1", byteArrayUtility.getBinaryString("1"));
    Assert.assertEquals("111", byteArrayUtility.getBinaryString("7"));
    Assert.assertEquals("0", byteArrayUtility.getBinaryString("0"));
    Assert.assertEquals("101111101011110000011111111", byteArrayUtility.getBinaryString("99999999"));
  }

  @Test
  public void byteArrayGenerationTest() {
    String val = "99999999";
//    Assert.assertArrayEquals(tokenGenerationHelper.formatByteArray(UInt32.getByte(val),
//            27), byteArrayUtility.formatByteArray(val, 27));
//    Assert.assertArrayEquals(tokenGenerationHelper.formatByteArray(UInt32.getByte("50"),
//            27), byteArrayUtility.formatByteArray("50", 27));
//    Assert.assertArrayEquals(tokenGenerationHelper.formatByteArray(UInt32.getByte("5000"),
//            5), byteArrayUtility.formatByteArray("5000", 5));
//    Assert.assertArrayEquals(tokenGenerationHelper.formatByteArray(UInt32.getByte("50"),
//            27), byteArrayUtility.getByteArray("50", 27));
  }

  @Test
  public void padBinaryStringTest() {
    Assert.assertEquals("55555111", byteArrayUtility.padBinaryString("111", 8, "5"));
    Assert.assertEquals("00000000", byteArrayUtility.padBinaryString("", 8, "0"));
    Assert.assertEquals("0001", byteArrayUtility.padBinaryString("1", 4, "0"));
  }

  @Test
  public void byteArrayToBinaryStringTest() {
    byte[] b = new byte[]{0xf, 0x8};
    Assert.assertEquals("10001111", byteArrayUtility.getBinaryString(b));
  }

  @Test
  public void byteArrayToPaddedBinaryStringTest() {
    byte[] b = new byte[]{0x0, 0x8};
    Assert.assertEquals("0000100000000000", byteArrayUtility.getPaddedBinaryString(b));
  }

  @Test
  public void formatByteArrayTest() {
    byte[] expected = new byte[]{0x1, 0x1, 0x1, 0x1};
    Assert.assertArrayEquals(tokenGenerationHelper.formatByteArray(expected, 4), byteArrayUtility.formatByteArray(expected, 4));
  }

  public static void main(String[] args){
    ByteArrayUtility byteArrayUtility = new ByteArrayUtility();
    TokenGenerationHelper tokenGenerationHelper = new TokenGenerationHelper();
    byte[] expected = new byte[]{(byte)124};

//    System.out.println(Arrays.toString(tokenGenerationHelper.formatByteArray(UInt32.getByte("124"), 8)));
//    System.out.println(byteArrayUtility.byteArrayToPaddedBinaryString(expected));
//    System.out.println(byteArrayUtility.getBinaryString("124"));
    System.out.println(Arrays.toString(byteArrayUtility.formatByteArray("78", 4)));
  }
}
