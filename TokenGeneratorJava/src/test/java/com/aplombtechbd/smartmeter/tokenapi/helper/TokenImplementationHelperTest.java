package com.aplombtechbd.smartmeter.tokenapi.helper;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TokenImplementationHelperTest {
  private TokenGenerationHelper tokenGenerationHelper;

  public TokenImplementationHelperTest() {
    tokenGenerationHelper = new TokenGenerationHelper();
  }

  @Test
  public void hasDecimalPointTest() {
    assertEquals(true, tokenGenerationHelper.hasDecimalPoint(100.5));
    assertEquals(false, tokenGenerationHelper.hasDecimalPoint(100));
    assertEquals(true, tokenGenerationHelper.hasDecimalPoint(.5));
    assertEquals(false, tokenGenerationHelper.hasDecimalPoint(0));
  }

  @Test
  public void padTokenBigIntegerTest() {
    assertEquals("00000000001111111111", tokenGenerationHelper.padTokenBigInteger(20, "1111111111"));
    assertEquals("00000000001111111111", tokenGenerationHelper.padTokenBigInteger(20, "1111111111"));
    assertEquals("11111111111111111111", tokenGenerationHelper.padTokenBigInteger(20, "11111111111111111111"));
  }

  @Test
  public void getSeqNoTest() {
    assertEquals(130916, tokenGenerationHelper.getSeqNo(511, 100));
    assertEquals(130916, tokenGenerationHelper.getSeqNo(65535, 100));
    assertEquals(131016, tokenGenerationHelper.getSeqNo(511, 200));
    assertEquals(25700, tokenGenerationHelper.getSeqNo(100, 100));
  }
}
