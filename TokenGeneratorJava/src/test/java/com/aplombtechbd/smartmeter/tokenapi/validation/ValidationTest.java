package com.aplombtechbd.smartmeter.tokenapi.validation;

import org.junit.Test;

import static junit.framework.TestCase.*;

public class ValidationTest {
  private Validator validator;

  public ValidationTest() {
    validator = new Validator();
  }

  @Test
  public void isValidMeterNo() {
    assertEquals(false, validator.isValidMeterNo("111111111.11"));
    assertEquals(true, validator.isValidMeterNo("11111111111"));
    assertEquals(false, validator.isValidMeterNo("111KADKSJD1"));
    assertEquals(false, validator.isValidMeterNo("           "));
    assertEquals(false, validator.isValidMeterNo("ff1a8b4edf1"));
    assertEquals(true, validator.isValidMeterNo("00000000000"));
    assertEquals(false, validator.isValidMeterNo("12312"));
    assertEquals(false, validator.isValidMeterNo("12345678912341323"));
  }

  @Test
  public void isValidSgc() {
    assertEquals(false, validator.isValidSgc("1111.11"));
    assertEquals(true, validator.isValidSgc("111111"));
    assertEquals(false, validator.isValidSgc("ffffff"));
    assertEquals(false, validator.isValidSgc("      "));
    assertEquals(true, validator.isValidSgc("000000"));
    assertEquals(true, validator.isValidSgc("000000000"));
    assertEquals(true, validator.isValidSgc("12312"));
    assertEquals(true, validator.isValidSgc("999999"));
  }

  @Test
  public void isValidKeyVersionNumberTest() {
    assertEquals(false, validator.isValidKeyVersionNumber(11));
    assertEquals(true, validator.isValidKeyVersionNumber(1));
    assertEquals(false, validator.isValidKeyVersionNumber(15));
    assertEquals(false, validator.isValidKeyVersionNumber(99));
    assertEquals(true, validator.isValidKeyVersionNumber(0));
    assertEquals(true, validator.isValidKeyVersionNumber(000000000));
    assertEquals(false, validator.isValidKeyVersionNumber(49));
    assertEquals(true, validator.isValidKeyVersionNumber(9));
  }

  @Test
  public void isValidKeyExpiredTime() {
    assertEquals(false, validator.isValidKeyExpiredTime(111111));
    assertEquals(true, validator.isValidKeyExpiredTime(9));
    assertEquals(true, validator.isValidKeyExpiredTime(10));
    assertEquals(true, validator.isValidKeyExpiredTime(15));
    assertEquals(true, validator.isValidKeyExpiredTime(000000));
    assertEquals(true, validator.isValidKeyExpiredTime(000000000));
    assertEquals(false, validator.isValidKeyExpiredTime(1234));
    assertEquals(true, validator.isValidKeyExpiredTime(255));
    assertEquals(false, validator.isValidKeyExpiredTime(256));
    assertEquals(true, validator.isValidKeyExpiredTime(1));
  }

  @Test
  public void isValidTariffIndex() {
    assertEquals(true, validator.isValidTariffIndex(99));
    assertEquals(true, validator.isValidTariffIndex(0));
    assertEquals(false, validator.isValidTariffIndex(999));
    assertEquals(false, validator.isValidTariffIndex(100));
    assertEquals(true, validator.isValidTariffIndex(1));
    assertEquals(true, validator.isValidTariffIndex(00));
    assertEquals(false, validator.isValidTariffIndex(111111));
    assertEquals(true, validator.isValidTariffIndex(10));
  }

  @Test
  public void isValidKeyNo() {
    assertEquals(true, validator.isValidKeyNo(99));
    assertEquals(true, validator.isValidKeyNo(0));
    assertEquals(true, validator.isValidKeyNo(999));
    assertEquals(false, validator.isValidKeyNo(65536));
    assertEquals(true, validator.isValidKeyNo(65535));
    assertEquals(true, validator.isValidKeyNo(00));
    assertEquals(false, validator.isValidKeyNo(11111111));
    assertEquals(true, validator.isValidKeyNo(1));
  }

  @Test
  public void isValidSeqNo() {
    assertEquals(true, validator.isValidSeqNo(99));
    assertEquals(false, validator.isValidSeqNo(0));
    assertEquals(true, validator.isValidSeqNo(255));
    assertEquals(false, validator.isValidSeqNo(256));
    assertEquals(true, validator.isValidSeqNo(1));
    assertEquals(false, validator.isValidSeqNo(00));
    assertEquals(false, validator.isValidSeqNo(111111));
    assertEquals(true, validator.isValidSeqNo(10));
  }

  @Test
  public void isValidAmount() {
    assertEquals(true, validator.isValidAmount(99999999));
    assertEquals(false, validator.isValidAmount(-1));
    assertEquals(false, validator.isValidAmount(999999999));
    assertEquals(true, validator.isValidAmount(0));
    assertEquals(false, validator.isValidAmount(-99999999));
    assertEquals(false, validator.isValidAmount(100000000));

  }

  @Test
  public void isValidHourTest(){
    assertEquals(false, validator.isValidHour(new int[]{}));
    assertEquals(true, validator.isValidHour(new int[]{0, 0}));
    assertEquals(true, validator.isValidHour(new int[]{0, 23}));
    assertEquals(true, validator.isValidHour(new int[]{23, 0}));
    assertEquals(false, validator.isValidHour(new int[]{24, -1}));
    assertEquals(false, validator.isValidHour(new int[]{-1, 24}));
    assertEquals(false, validator.isValidHour(new int[]{0, -24}));
    assertEquals(false, validator.isValidHour(new int[]{0, -23}));
    assertEquals(true, validator.isValidHour(new int[]{1}));
    assertEquals(true, validator.isValidHour(new int[]{1,2,3}));
    assertEquals(false, validator.isValidHour(new int[]{1,2,3,4,5}));
  }

  @Test
  public void isValidActivationModelTest(){
    assertEquals(true, validator.isValidActivatingModel(1));
    assertEquals(true, validator.isValidActivatingModel(0));
    assertEquals(false, validator.isValidActivatingModel(11));
    assertEquals(false, validator.isValidActivatingModel(2));
    assertEquals(false, validator.isValidActivatingModel(-1));
    assertEquals(true, validator.isValidActivatingModel(00));
  }

  @Test
  public void isDateValidTest(){
    assertEquals(true, validator.isValidDate("2018-01-22"));
    assertEquals(true, validator.isValidDate("1111-01-22"));
    assertEquals(false, validator.isValidDate("20188-01-22"));
    assertEquals(false, validator.isValidDate("2018-011-22"));
    assertEquals(false, validator.isValidDate("2018-01-221"));
    assertEquals(false, validator.isValidDate("0000-00-00"));
  }

  @Test
  public void isValidMaxPowerLimitTest(){
    assertEquals(false, validator.isValidMaxPowerLimit(new int[]{}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{1}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{2046}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{1, 2046}));
    assertEquals(false, validator.isValidMaxPowerLimit(new int[]{1, 2047}));
    assertEquals(false, validator.isValidMaxPowerLimit(new int[]{-1, 2046}));
    assertEquals(false, validator.isValidMaxPowerLimit(new int[]{1, -2046}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{1, 1}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{2046, 1}));
    assertEquals(true, validator.isValidMaxPowerLimit(new int[]{2046, 1, 1, 2}));
  }

  @Test
  public void isValidRates(){
    assertEquals(false, validator.isValidRates(new int[]{}));
    assertEquals(true, validator.isValidRates(new int[]{1}));
    assertEquals(true, validator.isValidRates(new int[]{8191}));
    assertEquals(true, validator.isValidRates(new int[]{1, 8191}));
    assertEquals(false, validator.isValidRates(new int[]{1, 8192}));
    assertEquals(false, validator.isValidRates(new int[]{-1, 8191}));
    assertEquals(false, validator.isValidRates(new int[]{0, -8191}));
    assertEquals(true, validator.isValidRates(new int[]{1, 1}));
    assertEquals(true, validator.isValidRates(new int[]{8191, 1}));
    assertEquals(true, validator.isValidRates(new int[]{8191, 8191, 1, 2}));
    assertEquals(false, validator.isValidRates(new int[]{8191, 8191, 1, 2, 3 ,4, 5, 6, 7, 8, 9}));
    assertEquals(false, validator.isValidRates(new int[]{1, 2, 3 ,4, 5, 6, 7, 8, 9,10}));
    assertEquals(true, validator.isValidRates(new int[]{1, 2, 3 ,4, 5, 6, 7, 8, 9}));
  }

  @Test
  public void isValidValidateTest(){
    assertEquals(false, validator.isValidValidate(-1));
    assertEquals(false, validator.isValidValidate(8));
    assertEquals(true, validator.isValidValidate(0));
    assertEquals(true, validator.isValidValidate(7));
    assertEquals(true, validator.isValidValidate(4));
  }

  @Test
  public void isValidTimesTest(){
    assertEquals(false, validator.isValidTimes(new int[]{}));
    assertEquals(true, validator.isValidTimes(new int[]{0}));
    assertEquals(true, validator.isValidTimes(new int[]{23}));
    assertEquals(true, validator.isValidTimes(new int[]{0, 23}));
    assertEquals(false, validator.isValidTimes(new int[]{0, 24}));
    assertEquals(false, validator.isValidTimes(new int[]{-1, 23}));
    assertEquals(false, validator.isValidTimes(new int[]{0, -23}));
    assertEquals(true, validator.isValidTimes(new int[]{0, 0}));
    assertEquals(true, validator.isValidTimes(new int[]{23, 1}));
    assertEquals(true, validator.isValidTimes(new int[]{23, 23, 0, 2}));
    assertEquals(false, validator.isValidTimes(new int[]{23, 23, 1, 2, 3 ,4, 5, 6, 7, 8, 9}));
    assertEquals(false, validator.isValidTimes(new int[]{1, 2, 3 ,4, 5, 6, 7, 8, 9, 10}));
    assertEquals(true, validator.isValidTimes(new int[]{1, 2, 3 ,4, 5, 6, 7, 8, 9}));
  }

  @Test
  public void isTouTariffValidRate(){
    assertEquals(false, validator.isValidSingleTariffRate(0));
    assertEquals(false, validator.isValidSingleTariffRate(8192));
    assertEquals(true, validator.isValidSingleTariffRate(1));
    assertEquals(true, validator.isValidSingleTariffRate(8191));
  }

  @Test
  public void isValidStepsTest(){
    assertEquals(false, validator.isValidSteps(new int[]{}));
    assertEquals(true, validator.isValidSteps(new int[]{1}));
    assertEquals(true, validator.isValidSteps(new int[]{4095}));
    assertEquals(true, validator.isValidSteps(new int[]{0}));
    assertEquals(false, validator.isValidSteps(new int[]{4096}));
    assertEquals(false, validator.isValidSteps(new int[]{1,2,3,4,5,6,7,8,9}));
    assertEquals(true, validator.isValidSteps(new int[]{1,2,3,4,5,6,7,8}));
  }

  @Test
  public void  isValidFlagTest(){
    assertEquals(true, validator.isValidFlag(new int[]{1, 1, 1, 1, 1, 1, 1, 1}));
    assertEquals(true, validator.isValidFlag(new int[]{1,1,1,1,1,1,1,1}));
    assertEquals(false, validator.isValidFlag(new int[]{1,1,1}));
    assertEquals(true, validator.isValidFlag(new int[]{0,0,0,0,0,0,0,0}));
    assertEquals(false, validator.isValidFlag(new int[]{0,0,0,0,0,0,0,0,0,0}));
    assertEquals(false, validator.isValidFlag(new int[]{2,2,2,1,1,1,1,1}));
  }

  @Test
  public void  isValidHolidaysTest(){
    assertEquals(true, validator.isValidHolidays(new String[]{"2017-02-14", "2000-11-12", "2000-01-30"}));
    assertEquals(false, validator.isValidHolidays(new String[]{}));
    assertEquals(true, validator.isValidHolidays(new String[]{"1999-01-12"}));
    assertEquals(false, validator.isValidHolidays(new String[]{"1","2000-12-12"}));
    assertEquals(true, validator.isValidHolidays(new String[]{"2000-12-12","2099-10-11", "2018-01-29"}));
    assertEquals(false, validator.isValidHolidays(new String[]{"0"}));
    assertEquals(false, validator.isValidHolidays(new String[]{
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29","2000-12-12","2099-10-11", "2018-01-29",
            "2018-01-29"
    }));
  }

  @Test
  public void isValidHolidayMode(){
    assertEquals(false, validator.isValidHolidayMode(2));
    assertEquals(true, validator.isValidHolidayMode(1));
    assertEquals(true, validator.isValidHolidayMode(0));
    assertEquals(false, validator.isValidHolidayMode(-1));
  }

  @Test
  public void isValidNumOfAllowableDaysTest(){
    assertEquals(false, validator.isValidNumOfAllowableDays(100));
    assertEquals(true, validator.isValidNumOfAllowableDays(1));
    assertEquals(true, validator.isValidNumOfAllowableDays(0));
    assertEquals(false, validator.isValidNumOfAllowableDays(-1));
    assertEquals(true, validator.isValidNumOfAllowableDays(99));
  }

  @Test
  public void isValidFriendModeHoursTest(){
    assertEquals(false, validator.isValidFriendModeHours(new int[]{}));
    assertEquals(false, validator.isValidFriendModeHours(new int[]{1}));
    assertEquals(false, validator.isValidFriendModeHours(new int[]{1, 2, 3}));
    assertEquals(false, validator.isValidFriendModeHours(new int[]{0, 24}));
    assertEquals(false, validator.isValidFriendModeHours(new int[]{-1, 23}));
    assertEquals(true, validator.isValidFriendModeHours(new int[]{0, 0}));
    assertEquals(true, validator.isValidFriendModeHours(new int[]{0, 23}));
  }

  @Test
  public void isValidFriendModeTest(){
    assertEquals(false, validator.isValidFriendMode(2));
    assertEquals(false, validator.isValidFriendMode(-1));
    assertEquals(true, validator.isValidFriendMode(0));
    assertEquals(true, validator.isValidFriendMode(1));
  }

  @Test
  public void isValidFriendModeDaysTest(){
    assertEquals(false, validator.isValidFriendModeDays(new int[]{}));
    assertEquals(false, validator.isValidFriendModeDays(new int[]{-1}));
    assertEquals(false, validator.isValidFriendModeDays(new int[]{7}));
    assertEquals(true, validator.isValidFriendModeDays(new int[]{0}));
    assertEquals(true, validator.isValidFriendModeDays(new int[]{0,1,2,3,4,5,6}));
    assertEquals(false, validator.isValidFriendModeDays(new int[]{0,1,2,3,4,5,6,6}));
  }

  @Test
  public void isValidMeterMode(){
    assertEquals(true, validator.isValidMeterMode(1));
    assertEquals(true, validator.isValidMeterMode(0));
    assertEquals(false, validator.isValidMeterMode(2));
    assertEquals(false, validator.isValidMeterMode(-1));
  }

  @Test
  public void isValidManufacturerID() {
    assertTrue(validator.isValidManufacturerID(99));
    assertTrue(validator.isValidManufacturerID(0));
    assertFalse(validator.isValidManufacturerID(100));
    assertFalse(validator.isValidManufacturerID(-1));
  }

  @Test
  public void isValidControl() {
    assertFalse(validator.isValidControl(99));
    assertTrue(validator.isValidControl(0));
    assertFalse(validator.isValidControl(37));
    assertTrue(validator.isValidControl(36));
  }
}
