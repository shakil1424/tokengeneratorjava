package com.aplombtechbd.smartmeter.tokenapi.helper;

import org.junit.Assert;
import org.junit.Test;

public class FriendModeTokenGenerationHelperTest {
  FriendModeTokenGenerationHelper friendModeTokenGenerationHelper;

  public FriendModeTokenGenerationHelperTest(){
    friendModeTokenGenerationHelper = new FriendModeTokenGenerationHelper();
  }

  @Test
  public void getWeekDaysBytesTest(){
    byte[] expected = new byte[]{0x0,0x0,0x0,0x0,0x0,0x1,0x1};
    Assert.assertArrayEquals(expected, friendModeTokenGenerationHelper.getWeekDaysBytes(new int[]{0,1,2,3,4}));
    Assert.assertArrayEquals(new byte[]{0x1,0x1,0x0,0x0,0x0,0x0,0x0},
            friendModeTokenGenerationHelper.getWeekDaysBytes(new int[]{2,3,4,5,6}));
  }
}
